<?php

// {
//     "reject": [
//       "Reject Reason 1",
//       "Reject Reason 2",
//       "Reject Reason 3",
//       "Reject Reason 4"
//     ],
//     "cancelAccepted": [
//       "cancelAccepted Reason 1",
//       "cancelAccepted Reason 2",
//       "cancelAccepted Reason 3",
//       "cancelAccepted Reason 4"
//     ],
//     "cancelOutForDelivery": [
//       "cancelOutForDelivery Reason 1",
//       "cancelOutForDelivery Reason 2",
//       "cancelOutForDelivery Reason 3",
//       "cancelOutForDelivery Reason 4"
//     ]
// }


$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramClientLastSavedTime = $requestParams->vendor_saved_time;

$objResponse = array();

$serverLastSavedTime = "2020-07-01 00:00:00";
if($paramClientLastSavedTime == $serverLastSavedTime) {
       $objResponse["status"] = "noupdate";
} else {
    //Reject English
    $arrReject             = array("Ordered items are out of stock", 
                                   "Currently not taking any orders", 
                                   "Rejecting the order because of the customer");
    $objResponse["reject"] = array();
    foreach ($arrReject as $reject) {
        array_push($objResponse["reject"], $reject);
    }

    // //Reject Hindi
    // $arrRejectHindi             = array("Reject Reason 1 Hindi", 
    //                                "Reject Reason 2", 
    //                                "Reject Reason 3", 
    //                                "Reject Reason 4");
    // $objResponse["reject_hindi"] = array();
    // foreach ($arrRejectHindi as $reject) {
    //  array_push($objResponse["reject_hindi"], $reject);
    // }

    // //Reject Marathi
    // $arrRejectMarathi            = array("Reject Reason 1 Marathi", 
    //                                "Reject Reason 2", 
    //                                "Reject Reason 3", 
    //                                "Reject Reason 4");
    // $objResponse["reject_marathi"] = array();
    // foreach ($arrRejectMarathi as $reject) {
    //  array_push($objResponse["reject_marathi"], $reject);
    // }

    //Cancel Accepted English
    $arrCancelAccepted = array("Customer didn't arrive to pick up the order",
                               "Ordered items are currently out of stock");
    $objResponse["cancelAccepted"] = array();
    foreach ($arrCancelAccepted as $cancelAccepted) {
        array_push($objResponse["cancelAccepted"], $cancelAccepted);
    }

    // //Cancel Accepted Hindi
    // $arrCancelAcceptedHindi = array("cancelAccepted Reason 1 Hindi",
    //                            "cancelAccepted Reason 2", 
    //                            "cancelAccepted Reason 3", 
    //                            "cancelAccepted Reason 4");
    // $objResponse["cancelAccepted_hindi"] = array();
    // foreach ($arrCancelAcceptedHindi as $cancelAccepted) {
    //  array_push($objResponse["cancelAccepted_hindi"], $cancelAccepted);
    // }

    // //Cancel Accepted Marathi
    // $arrCancelAcceptedMarathi = array("cancelAccepted Reason 1 Marathi",
    //                            "cancelAccepted Reason 2", 
    //                            "cancelAccepted Reason 3", 
    //                            "cancelAccepted Reason 4");
    // $objResponse["cancelAccepted_marathi"] = array();
    // foreach ($arrCancelAcceptedMarathi as $cancelAccepted) {
    //  array_push($objResponse["cancelAccepted_marathi"], $cancelAccepted);
    //}

    //Cancel Out for delivery English
    $arrCancelOutForDelivery = array("Customer not available at the provided address",
                                     "Customer declined to take the order", 
                                     "Couldn't find the provided address");
    $objResponse["cancelOutForDelivery"] = array();
    foreach ($arrCancelOutForDelivery as $cancel) {
        array_push($objResponse["cancelOutForDelivery"], $cancel);
    }

    // //Cancel Out for delivery Hindi
    // $arrCancelOutForDeliveryHindi = array("cancelOutForDelivery Reason 1 Hindi",
    //                                  "cancelOutForDelivery Reason 2", 
    //                                  "cancelOutForDelivery Reason 3",
    //                                  "cancelOutForDelivery Reason 4");
    // $objResponse["cancelOutForDelivery_hindi"] = array();
    // foreach ($arrCancelOutForDeliveryHindi as $cancel) {
    //  array_push($objResponse["cancelOutForDelivery_hindi"], $cancel);
    // }

    // //Cancel Out for delivery Marathi
    // $arrCancelOutForDeliveryMarathi = array("cancelOutForDelivery Reason 1 Marathi",
    //                                  "cancelOutForDelivery Reason 2", 
    //                                  "cancelOutForDelivery Reason 3",
    //                                  "cancelOutForDelivery Reason 4");
    // $objResponse["cancelOutForDelivery_marathi"] = array();
    // foreach ($arrCancelOutForDeliveryMarathi as $cancel) {
    //  array_push($objResponse["cancelOutForDelivery_marathi"], $cancel);
    // }

    $objResponse["status"] = "success";
    $objResponse["server_time"] = $serverLastSavedTime;
}
echo json_encode($objResponse);
