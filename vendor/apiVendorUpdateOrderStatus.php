<?php
//22 keys in request body
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramVendorCode = $requestParams->vendor_code;
$paramOrderId = $requestParams->order_id;
$paramOrderStatus = $requestParams->order_status; //Refer VENDOR_CONSTANTS.php file

$paramReasonOfRejection = $requestParams->rejection_reason; //if vendor rejects due to some reason
$paramReasonOfCancellation = $requestParams->cancellation_reason; //In case when customer fails to pick up the order

$paramCustomerMobileNo      = $requestParams->customer_mob_no;
$paramVendorCategory        = $requestParams->vendor_category;

$paramHasOptForHomeDelivery = $requestParams->has_opt_for_home_delivery;
$paramOrderTotalAmount      = $requestParams->order_total_amount;

$paramCustomerName    = $requestParams->customer_name;
$paramDeliveryCharges = $requestParams->delivery_charges;
$paramMinWaitingTime  = $requestParams->min_waiting_time_in_minutes;
$paramVendorName      = $requestParams->vendor_name;
$paramVendorMobileNo  = $requestParams->vendor_mob_no;

$paramHasOptForHomeDelivery = filter_var($paramHasOptForHomeDelivery , FILTER_VALIDATE_BOOLEAN); 
if($paramHasOptForHomeDelivery) {
    $paramCurrLat  = $requestParams->latitude;
    $paramCurrLong = $requestParams->longitude;
    $paramAddress  = $requestParams->address;
    $paramLocality = $requestParams->locality;
    $paramCity     = $requestParams->city;
    $paramState    = $requestParams->state;
    $paramCountry  = $requestParams->country;
    $paramPinCode  = $requestParams->pincode;
}

$objResponse = array();
if ($paramVendorCode == null || $paramOrderId == null 
    || $paramOrderStatus == null) {
    //error
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 3 required";
} else {
    require_once './VENDOR_CONSTANTS.php';
    require_once './utils.php';
    require_once './vendor_db_functions.php';
    $objOfVendorDbFunc = new vendor_db_functions();
    $objConn           = $objOfVendorDbFunc->getConnectionObj();

    $paramVendorCategory = strtolower($paramVendorCategory);
    
    $currentTime = utilsGetDateAndTimeInIST();
    $doesOrderIdAndVendorCodeMatch = $objOfVendorDbFunc->isOrderIdAssociatedWithSameVendorCode($objConn,
     $paramVendorCode, $paramOrderId);
    if ($doesOrderIdAndVendorCodeMatch) {
        $notiMsg = "There is an update for your order.";
        $orderStatus = STR_PENDING;
        switch($paramOrderStatus) {
            case STR_PENDING:
                //would never come pending in this api
            break;
            case STR_CANCELLED:
                //Check if the order is in STR_ACCEPTED or STR_OUT_FOR_DELIVERY state 
                $arrOfOrderStatus = array(STR_ACCEPTED, STR_OUT_FOR_DELIVERY);
                $isOrderInParticularState = $objOfVendorDbFunc->isOrderInParticularStatus($objConn,
                 $paramOrderId, $arrOfOrderStatus);
                if($isOrderInParticularState) {
                    $notiMsg = "The order is canclelled by the vendor.";
                    $orderStatus = STR_CANCELLED;
                    $updateOrderStatus = $objOfVendorDbFunc->updateOrderStatus($objConn,
                     $paramOrderId, $orderStatus, $currentTime, $paramReasonOfCancellation);
                } else {
                    echoErrorResponseAndExit("504","Dear Vendor, the order is yet to be accepted. It is done from the pending tab.");
                }
            break;
            case STR_REJECTED:
                $notiMsg = "The order is rejected by the vendor.";
                //Check if the order is NOT in cancelled state 
                $arrOfOrderStatus = array(STR_CANCELLED);
                $isOrderInNotParticularState = $objOfVendorDbFunc->isOrderNotInParticularStatus($objConn,
                 $paramOrderId, $arrOfOrderStatus);
                if($isOrderInNotParticularState) {
                    $orderStatus = STR_REJECTED;
                    $updateOrderStatus = $objOfVendorDbFunc->updateOrderStatus($objConn,
                     $paramOrderId, $orderStatus, $currentTime, null, $paramReasonOfRejection);
                } else {
                    echoErrorResponseAndExit("505","Dear Vendor, the order has been cancelled by the customer.");
                }
            break;
            case STR_ACCEPTED:
                $notiMsg = "The order is accepted by the vendor.";
                //Check if the order is NOT in cancelled state 
                $arrOfOrderStatus = array(STR_CANCELLED);
                $isOrderInNotParticularState = $objOfVendorDbFunc->isOrderNotInParticularStatus($objConn,
                 $paramOrderId, $arrOfOrderStatus);
                if($isOrderInNotParticularState) {
                    $orderStatus = STR_ACCEPTED;
                    $updateOrderStatus = $objOfVendorDbFunc->updateOrderStatus($objConn,
                     $paramOrderId, $orderStatus, $currentTime);
                } else {
                    echoErrorResponseAndExit("506","Dear Vendor, the order has been cancelled by the customer.");
                }  
            break;
            case STR_OUT_FOR_DELIVERY:
                $notiMsg = "The order will be delivered in some time.";
                //Check if the order is in STR_ACCEPTED state 
                $arrOfOrderStatus = array(STR_ACCEPTED);
                $isOrderInParticularState = $objOfVendorDbFunc->isOrderInParticularStatus($objConn,
                 $paramOrderId, $arrOfOrderStatus);
                if($isOrderInParticularState) {
                     $orderStatus = STR_OUT_FOR_DELIVERY;
                     $updateOrderStatus = $objOfVendorDbFunc->updateOrderStatus($objConn,
                      $paramOrderId, $orderStatus, $currentTime);
                } else {
                    echoErrorResponseAndExit("507","Dear Vendor, the order is yet to be accepted. It is done from the pending tab.");
                }
            break;
            case STR_COMPLETED:
                $notiMsg = "Happy shopping! Hope you have received your packet.";
                //Check if the order is in STR_ACCEPTED or STR_OUT_FOR_DELIVERY state 
                $arrOfOrderStatus = array(STR_ACCEPTED, STR_OUT_FOR_DELIVERY);
                $isOrderInParticularState = $objOfVendorDbFunc->isOrderInParticularStatus($objConn,
                 $paramOrderId, $arrOfOrderStatus);
                if($isOrderInParticularState) {
                     $orderStatus = STR_COMPLETED;
                     $updateOrderStatus = $objOfVendorDbFunc->updateOrderStatus($objConn,
                      $paramOrderId, $orderStatus, $currentTime);
                } else {
                    echoErrorResponseAndExit("508","Dear Vendor, the order is still in process.");
                }
            break;
            default:
                echoErrorResponseAndExit("503","Order Status not found");
            break;
        }

        if($updateOrderStatus) {
            $insertNewStatusInDetailsTable = $objOfVendorDbFunc->
            insertOrderNewStatusInOrderStatusDetails($objConn, $paramOrderId, 
                                     $paramCustomerMobileNo, $paramVendorCode, $paramVendorCategory,
                                     $paramHasOptForHomeDelivery, $paramOrderTotalAmount, 
                                     $paramCustomerName, $paramMinWaitingTime, 
                                     $paramVendorName, $paramVendorMobileNo, $orderStatus,
                                     $paramReasonOfCancellation, $paramReasonOfRejection,
                                     $paramCurrLat, $paramCurrLong, $paramAddress,
                                     $paramLocality, $paramCity, $paramState, $paramCountry, $paramPinCode,
                                     $paramDeliveryCharges);

            $objResponse["status"]                    = "success";
            $objResponse["order_status_changed_to"]   =  $orderStatus;

            $fcmToken = $objOfVendorDbFunc->getCustomerFcmToken($objConn, $paramCustomerMobileNo);
            if($fcmToken != null) {
                utilsSendFCMMessageSingle($fcmToken, $notiMsg, 
                        "Update for Order No. " . $paramOrderId, FIREBASE_KEY_CUSTOMER, $orderStatus, $paramOrderStatus);
            }
        } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "1000";
            $objResponse["error_msg"]  =  "Shouldn't come here! Unable to update order status";
        }
    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "502";
        $objResponse["error_msg"]  = "This order id doesn't belong to this vendor";
    }

}
echo json_encode($objResponse);


function echoErrorResponseAndExit($errorCode, $errorMsg) {
    $objRes = array();
    $objRes["status"]     = "error";
    $objRes["error_code"] = $errorCode;
    $objRes["error_msg"]  = $errorMsg;
    echo json_encode($objRes);
    exit();
}
/*
{
	"vendor_code": "RJ-84326",
	"order_id": "9",
	"order_status" : "completed",
	"cancellation_reason":null,
	"rejection_reason" : null,
	"customer_mob_no":"9819509645",
	"vendor_category":"grocery",
	"has_opt_for_home_delivery":"false",
	"order_total_amount":"200",
	"customer_name":"CUST_NAME",
	"delivery_charges": 30,
	"min_waiting_time_in_minutes": 40,
	"vendor_name":"VENDOR_NAME",
	"vendor_mob_no": "9819509645",
    "latitude":72.121212,
    "longitude": 138.2111,
    "address": "address",
    "locality": "locality",
    "city": "Rajkot",
    "state": "Gujarat",
    "country": "India",
    "pincode": "1111104"
}
 */