<?php
//DONE
//all operations insert/ update and delete will be performed here
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramVendorCode = $requestParams->vendor_code;
$paramVendorCategory = $requestParams->vendor_category;
$paramVendorPreferences = $requestParams->item_list;
$objResponse = array();
if ($paramVendorCode == null || $paramVendorCategory == null 
    || count($paramVendorPreferences) == 0) {
    //error
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 3 required";
} else {

    $paramVendorCategory = strtolower($paramVendorCategory);

    require_once './vendor_db_functions.php';
    $objOfVendorDbFunc = new vendor_db_functions();
    $objConn = $objOfVendorDbFunc->getConnectionObj();
    
    $doesVendorCodeExists = $objOfVendorDbFunc->isVendorCodePresent($objConn, $paramVendorCode);
    if($doesVendorCodeExists){
        foreach($paramVendorPreferences as $paramObjItem){
            if(!performOperationOnEachItemObj($paramObjItem, $objOfVendorDbFunc, $objConn)){
                $objResponse["status"]     = "error";
                $objResponse["error_code"] = "1000";
                $objResponse["error_msg"]  = "Unknown error. Server side issue";
                echo json_encode($objResponse);
                exit();
            }
        }
        $objResponse["status"]     = "success";   
    } else {
        $objResponse["status"] = "error";
        $objResponse["error_code"] = "200";   
        $objResponse["error_msg"] = "invalid vendor code";   
    }
}
echo json_encode($objResponse);



function performOperationOnEachItemObj($objItem, $objOfVendorDbFunc, $objConn){

    $itemOperationType = $objItem->operation_type;
    $itemId = $objItem->item_id;
    $itemPrice = $objItem->item_price;
    $isItemInStock =  $objItem->is_available; 
    $isItemInStock = filter_var($isItemInStock , FILTER_VALIDATE_BOOLEAN); 
        switch($itemOperationType) {
            case "insert":
                $itemResponseLocal = fnPerformInsertOperation($objConn, $objOfVendorDbFunc,
                 $itemId, $itemPrice);
            break;
            case "update":
                $itemResponseLocal  = fnPerformUpdateOperation($objConn, $objOfVendorDbFunc,
                 $itemId, $itemPrice, $isItemInStock);
            break;
            case "delete":
                $itemResponseLocal  = fnPerformDeleteOperation($objConn, $objOfVendorDbFunc,
                 $itemId, $itemPrice);
            break;
            default:
                stopExecution();
            break;
        }
        return $itemResponseLocal;
}

function fnPerformInsertOperation($objConn, $objOfVendorDbFunc, $itemId, $itemPrice){
    //$itemResponse = array();
    global $paramVendorCode, $paramVendorCategory;
    $insertIntoDb = $objOfVendorDbFunc->insertVendorPreferenceItem($objConn, 
     $paramVendorCode, $itemId, $itemPrice, $paramVendorCategory);
    if($insertIntoDb){
        return true;
        // $itemResponse["status"]     = "success";
        // $itemResponse["operation"] = "insert";
    } else {
        return false;
        // $itemResponse["status"]     = "error";
        // $itemResponse["error_code"] = "101";
        // $itemResponse["error_msg"]  = "Unable to insert";
    }
    // $itemResponse["item_id"] = $itemId;
    // return $itemResponse;
}

function fnPerformUpdateOperation($objConn, $objOfVendorDbFunc, $itemId, $itemPrice, $isItemInStock){
    //$itemResponse = array();
    global $paramVendorCode, $paramVendorCategory;
    $updateDB = $objOfVendorDbFunc->updateVendorPreferenceItem($objConn, 
    $paramVendorCode, $itemId, $itemPrice, $paramVendorCategory, $isItemInStock);
   if($updateDB){
    //    $itemResponse["status"]     = "success";
    //    $itemResponse["operation"] = "update";
    return true;
   } else {
    return false;
    //    $itemResponse["status"]     = "error";
    //    $itemResponse["error_code"] = "103";
    //    $itemResponse["error_msg"]  = "Unable to update";
   }
//    $itemResponse["item_id"] = $itemId;
//    return $itemResponse;
}

function fnPerformDeleteOperation($objConn, $objOfVendorDbFunc, $itemId){
    //$itemResponse = array();
    global $paramVendorCode, $paramVendorCategory;
    $deleteDB = $objOfVendorDbFunc->deleteVendorPreferenceItem($objConn, 
    $paramVendorCode, $itemId, $paramVendorCategory);
   if($deleteDB){
    //    $itemResponse["status"]     = "success";
    //    $itemResponse["operation"] = "delete";
       return true;
    } else {
     return false;
    //    $itemResponse["status"]     = "error";
    //    $itemResponse["error_code"] = "102";
    //    $itemResponse["error_msg"]  = "Unable to delete";
   }
//    $itemResponse["item_id"] = $itemId;
//    return $itemResponse;
}

function stopExecution(){
$objResponse = array();
$objResponse["status"]     = "error";
$objResponse["error_code"] = "501";
$objResponse["error_msg"]  = "Key not found in json object";
echo json_encode($objResponse);
exit();

}