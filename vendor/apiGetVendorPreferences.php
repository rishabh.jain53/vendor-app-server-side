<?php
//DONE
$jsonObj = file_get_contents("php://input");
$requestParams = json_decode($jsonObj);
$paramVendorCode = $requestParams->vendor_code;
$paramVendorCategory = $requestParams->vendor_category;

$objResponse["item_list"] = array();
$itemObj = array();
if ($paramVendorCode == null || $paramVendorCategory == null) {
    //error
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 2 required";
} else {
    require_once './VENDOR_CONSTANTS.php';
    require_once './vendor_db_functions.php';
    $vendorDBObj = new vendor_db_functions();
    $objConn = $vendorDBObj->getConnectionObj();

    $paramVendorCategory = strtolower($paramVendorCategory);
    
    $doesVendorCodeExists = $vendorDBObj->isVendorCodePresent($objConn, $paramVendorCode);
    if($doesVendorCodeExists){
        $getVendorPreferenceList = $vendorDBObj->getVendorPreferenceListOfCategory($objConn,
                 $paramVendorCode, $paramVendorCategory);
        
        $objResponse["count"]    = mysqli_num_rows($getVendorPreferenceList);
        $objResponse["category"] = $paramVendorCategory;
        if(mysqli_num_rows($getVendorPreferenceList)>0) {
            $objResponse["status"]   = "success"; 
            while($row = mysqli_fetch_assoc($getVendorPreferenceList)){
                $itemObj["item_id"]            = $row["item_id"];
                $itemObj["item_price"]         = $row["item_price"];
                $itemObj["item_name_english"]  = $row["item_name_english"];
                $itemObj["item_name_hindi"]    = $row["item_name_hindi"];
                $itemObj["item_name_marathi"]  = $row["item_name_marathi"];
                $itemObj["item_sub_category"]  = $row["item_sub_category"];
                $itemObj["item_base_quantity"] = $row["item_base_quantity"];
                $itemObj["item_image_url"]     = $row["item_image_url_domain"] . $row["item_image_url"];
                $itemObj["item_description"]   = $row["item_description"];
                array_push($objResponse["item_list"], $itemObj);
            }
        } else {
            //no result found
            $objResponse["status"]      = "error"; 
            $objResponse["error_code"]  = "2000"; 
            $objResponse["error_msg"]   = "NO ITEMS FOUND"; 
        }

    } else {
        $objResponse["status"] = "error";
        $objResponse["error_code"] = "200";   
        $objResponse["error_msg"] = "invalid vendor code";   
    }
} 

echo json_encode($objResponse);

?>