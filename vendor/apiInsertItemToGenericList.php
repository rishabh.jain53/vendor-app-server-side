<?php
//DONE
$paramCategory     = $_POST["category"];  //vegetable_fruit or medical
$paramEnglishName  = $_POST["item_name_english"];
$paramHindiName    = $_POST["item_name_hindi"];
$paramMarathiName  = $_POST["item_name_marathi"];
$paramSubCategory  = $_POST["item_sub_category"];
$paramBaseQuantity = $_POST["item_base_quantity"];
$paramDescription  = $_POST["item_description"];

$paramImageUrl     = $_FILES["item_image"]["tmp_name"];

$objResponse = array();
if ($paramCategory == null || 
    $paramEnglishName == null || $paramHindiName == null || $paramMarathiName == null
    || $paramSubCategory == null || $paramImageUrl == null || $paramBaseQuantity == null ||
    $paramDescription == null) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 8 required";
} else {
    require_once './VENDOR_CONSTANTS.php';
    require_once './utils.php';
    require_once './vendor_db_functions.php';
    $objVDbF = new vendor_db_functions();
    $objConn = $objVDbF->getConnectionObj();

    $paramCategory = strtolower($paramCategory);

    $parentDirectory = "../";
    $imageDirectory = "images/";

    $imageName = $paramEnglishName . "_" . $paramCategory . "_" . $paramSubCategory;
    $ext = pathinfo($_FILES["item_image"]["name"], PATHINFO_EXTENSION);
    $imageNameWithExt = $imageName.".".$ext;
    $imageNameWithExt = replaceBlankWithUnderscore($imageNameWithExt);

    $locationofImageForDB = $imageDirectory . $imageNameWithExt;
    $locationOfImageInFileSystem = $parentDirectory . $locationofImageForDB;

    $isImageSaved = compressImageAndSave($paramImageUrl, $locationOfImageInFileSystem, 70);

    if($isImageSaved) {
        $paramEnglishName = ucfirst($paramEnglishName);

        $insertItem = $objVDbF->insertIntoGenericItemsTable($objConn,
        $paramEnglishName, $paramHindiName, $paramMarathiName,
        $paramSubCategory, $paramBaseQuantity, SERVER_URL,
        $locationofImageForDB, $paramCategory, $paramDescription);

        if ($insertItem) {
            $objResponse["status"] = "success";
            $objResponse["operation"] = "insert";
        } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "101";
            $objResponse["error_msg"]  = "Unable to insert";
        }

    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "110";
        $objResponse["error_msg"]  = " unable to save image on server";
    }
}

echo json_encode($objResponse);


                        

                        
