<?php

/*{
    "vendor_image": "",
    "vendor_code": "ddd",
    "is_profile_pic_updated": true,
    "is_time_slots_updated": true,
    "is_profile_updated": true,
    "vendor_details": {
      "vendor_name":"NAME",
      "nick_name": "Nickname",
      "is_delivery_available": true,
      "min_order_amount": "250",
      "delivery_charges": "40",
      "min_waiting_time": "30",
      "latitude": 0,
      "longitude": 0,
      "address": "address",
      "locality": "locality",
      "city": "Rajkot",
      "state": "Gujarat",
      "country": "India",
      "pincode": "1111104",
      "vendor_category":"grocery/medical"
    },
    "time_slots": [
      {
        "start_time": "08:00",
        "end_time": "12:00"
      },
      {
        "start_time": "04:00",
        "end_time": "07:30"
      }
    ]
   }*/

$paramVendorCode  = $_POST["vendor_code"];
$objResponse = array();
if($paramVendorCode==null){
    $objResponse["status"] = "error";
    $objResponse["error_code"] = "1";   
    $objResponse["error_msg"] = "Invalid request parameters 1 required";    
} else {
    require_once './VENDOR_CONSTANTS.php';
    require_once './utils.php';
    include_once "./vendor_db_functions.php";
    $vendorDBObj = new vendor_db_functions();
    $connObj = $vendorDBObj->getConnectionObj();

    $isVendorCodeValid = $vendorDBObj->isVendorCodePresent($connObj, $paramVendorCode);
    if($isVendorCodeValid){
        

        /* ======== VENDOR PROFILE UPDATE CODE ======== */
        $paramIsVendorProfileUpdated = $_POST["is_profile_pic_updated"];
        if($paramIsVendorProfileUpdated) {
            $imageTmpName  = $_FILES['vendor_image']['tmp_name'];
            if($imageTmpName!=null){
                $parentFolder = "vendor/";
                $file_path = "images/";
                $ext = pathinfo($_FILES["vendor_image"]["name"], PATHINFO_EXTENSION);
                $file_path_with_ext = $file_path . $paramVendorCode . "." . $ext;
                if(move_uploaded_file($imageTmpName, $file_path_with_ext)){
                    //update image url query
                    $vendorDBObj->updateVendorProfileURL($connObj, SERVER_URL,
                    $parentFolder . $file_path_with_ext, $paramVendorCode);
                    $objResponse["status"]    = "success";
                    $objResponse["operation"] = "profile picture update";

                } else {
                    $objResponse["status"]     = "error";
                    $objResponse["error_code"] = "110";
                    $objResponse["error_msg"]  = "110 - Failed to upload image";
                }
            } else {
                $vendorDBObj->updateVendorProfilePicToDefault($connObj, $paramVendorCode);
                $objResponse["status"]    = "success";
                $objResponse["operation"] = "profile picture removed";
            }
        }

        /* ======== VENDOR SETTINGS CODE ======== */
        $paramIsVendorSettingsUpdated = $_POST["is_profile_updated"];
        if($paramIsVendorSettingsUpdated){
            $jsonObj             = $_POST["vendor_details"];
            $paramVendorSettings = json_decode($jsonObj);

            $paramVendorName          = $paramVendorSettings->vendor_name;
            $paramNickName            = $paramVendorSettings->nick_name;
            $paramIsDeliveryAvailable = $paramVendorSettings->is_delivery_available;
            $paramIsDeliveryAvailable = filter_var($paramIsDeliveryAvailable , FILTER_VALIDATE_BOOLEAN); 
            $paramMinOrderAmount      = $paramVendorSettings->min_order_amount;
            $paramDeliveryCharges     = $paramVendorSettings->delivery_charges;
            $paramLat                 = $paramVendorSettings->latitude;
            $paramLong                = $paramVendorSettings->longitude;
            $paramMinWaitingTime      = $paramVendorSettings->min_waiting_time;

            $paramAddress             = $paramVendorSettings->address;
            $paramLocality            = $paramVendorSettings->locality;
            $paramCity                = $paramVendorSettings->city;
            $paramState               = $paramVendorSettings->state;
            $paramCountry             = $paramVendorSettings->country;
            $paramPinCode             = $paramVendorSettings->pincode;
            $paramVendorCategory      = $paramVendorSettings->vendor_category;
            $paramVendorCategory      = strtolower($paramVendorCategory);
            $currentTime              = utilsGetDateAndTimeInIST();
            
            $updateVendorQuery = $vendorDBObj->updateVendorSettings($connObj, $paramVendorCode, $paramVendorName,
             $paramNickName, $paramIsDeliveryAvailable, $paramMinOrderAmount, $paramDeliveryCharges, 
             $paramLat, $paramLong, $paramMinWaitingTime, $paramAddress, $paramLocality, $paramCity, $paramState,
             $paramCountry, $paramPinCode, $currentTime, $paramVendorCategory);

             if($updateVendorQuery){
                $objResponse["status"] = "success";
                $objResponse["operation"] = "Vendor settings";
             } else {
                $objResponse["status"] = "error";
                $objResponse["error_code"] = "103";   
                $objResponse["error_msg"] = "Unable to Update";  
                $objResponse["operation"] = "Vendor settings";
             }
        }

        /* ======== TIME SLOTS CODE ======== */
        $paramIsVendorSlotsUpdated = $_POST["is_time_slots_updated"];
        if($paramIsVendorSlotsUpdated) {
            $arrOfSlotsJsonObj = $_POST["time_slots"]; //array of json object 24 hr format
            $updateTimeSlots = $vendorDBObj->updateTimeSlotsOfVendor($connObj, 
             $paramVendorCode, $arrOfSlotsJsonObj); 
            if($updateTimeSlots) { 
                $objResponse["status"] = "success";
                $objResponse["operation"] = "Vendor time slots";
            } else {
                $objResponse["status"] = "error";
                $objResponse["error_code"] = "103";   
                $objResponse["error_msg"] = "Unable to update";  
                $objResponse["operation"] = "Vendor time slots";
            }
        }
    } else {
        $objResponse["status"] = "error";
        $objResponse["error_code"] = "200";   
        $objResponse["error_msg"] = "invalid vendor code";   
    }
}

echo json_encode($objResponse);
?>