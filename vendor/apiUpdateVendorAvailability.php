<?php
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramVendorCode = $requestParams->vendor_code;
$paramIsAvailable = $requestParams->is_vendor_available;
$objResponse = array();
if ($paramVendorCode == null || $paramIsAvailable == null) {
    //error
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 2 required";
} else {
    require_once './vendor_db_functions.php';
    $objOfVendorDbFunc = new vendor_db_functions();
    $objConn = $objOfVendorDbFunc->getConnectionObj();

    $doesVendorCodeExists = $objOfVendorDbFunc->isVendorCodePresent($objConn, $paramVendorCode);
    if($doesVendorCodeExists){
        require_once './utils.php';
        $currentTime              = utilsGetDateAndTimeInIST();

        $paramIsAvailable = filter_var($paramIsAvailable , FILTER_VALIDATE_BOOLEAN); 
        $updateAvailibility = $objOfVendorDbFunc->updateVendorAvailability($objConn,
         $paramVendorCode, $paramIsAvailable, $currentTime);
         if($updateAvailibility) {
            $objResponse["status"]     = "success";   
         } else {
            $objResponse["status"]     = "error";   
            $objResponse["error_code"] = "103";
            $objResponse["error_msg"]  = "Unable to update";
         }
        
    } else {
        $objResponse["status"] = "error";
        $objResponse["error_code"] = "200";   
        $objResponse["error_msg"] = "invalid vendor code";   
    }
}

echo json_encode($objResponse);

?>