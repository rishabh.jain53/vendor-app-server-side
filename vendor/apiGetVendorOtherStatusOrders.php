<?php
//TB tested
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramVendorCode = $requestParams->vendor_code;
$paramOrderStatus = $requestParams->order_status;
$paramOrderStatus = trim($paramOrderStatus);
$paramPageNo = $requestParams->page_no;
$offSetLimit = 10;
$offset = $paramPageNo * $offSetLimit;

$objResponse = array();
if($paramVendorCode == null || $paramOrderStatus == null) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 3 required";
} else {
    require_once './VENDOR_CONSTANTS.php';
    require_once './utils.php';
    require_once './vendor_db_functions.php';
    $vendorDBObj = new vendor_db_functions();
    $objConn = $vendorDBObj->getConnectionObj();

    $doesVendorCodeExists = $vendorDBObj->isVendorCodePresent($objConn, $paramVendorCode);
    if($doesVendorCodeExists){

        $getOrders = 0;
        switch($paramOrderStatus){
            case STR_ACCEPTED:
                $getOrders = $vendorDBObj->getAcceptedOrderDetailsOfVendor($objConn,
                 $paramVendorCode, $offset, $offSetLimit);
            break;
            case STR_COMPLETED:
                $getOrders = $vendorDBObj->getCompletedOrdersDetailsOfVendor($objConn,
                 $paramVendorCode, $offset, $offSetLimit);
            break;
            default:
                $objResponse["status"] = "error";
                $objResponse["error_code"] = "1";
                $objResponse["error_msg"]  = "Invalid request parameter status not found";
                echo json_encode($objResponse);
                exit();
            break;
        }
          
        $objResponse["status"] = "success";

        if (mysqli_num_rows($getOrders) > 0) {
            $formattedJson = utilsGetOrdersInJSONFormatNew($getOrders);
            $objResponse["count"] = count($formattedJson);
            $objResponse["orders"] = $formattedJson;
        } else {
            $objResponse["count"] = 0;
            $objResponse["orders"] = "No orders found";
        }
    } else {
        $objResponse["status"] = "error";
        $objResponse["error_code"] = "200";   
        $objResponse["error_msg"] = "invalid vendor code"; 
    }

}

echo json_encode($objResponse);

?>