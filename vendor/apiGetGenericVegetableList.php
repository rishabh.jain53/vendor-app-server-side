<?php
//DONE
$jsonObj       = file_get_contents('php://input');
$requestParams = json_decode($jsonObj);
$paramCategory = $requestParams->vendor_category; //vegetable_fruit and medical
$paramCategory = strtolower($paramCategory);

require_once './vendor_db_functions.php';
$objVDbF = new vendor_db_functions();
$objConn = $objVDbF->getConnectionObj();

$queryResultCategoryList = $objVDbF->getGenericListOfCategory($objConn, $paramCategory);

$rowsCount = mysqli_num_rows($queryResultCategoryList);
if ($rowsCount > 0) {
    
    $itemObj                           = array();
    $responseArrOfObjects["item_list"] = array();

    $responseArrOfObjects["count"]    = $rowsCount;
    $responseArrOfObjects["status"]   = "success";
    $responseArrOfObjects["category"] = $paramCategory;

    while ($row = mysqli_fetch_assoc($queryResultCategoryList)) {
        $itemObj["item_id"]            = $row["item_id"];
        $itemObj["item_name_english"]  = $row["item_name_english"];
        $itemObj["item_name_hindi"]    = $row["item_name_hindi"]; // gets 'escaped unicode' text, convert it to UTF-8
        $itemObj["item_name_marathi"]  = $row["item_name_marathi"];
        $itemObj["item_sub_category"]  = $row["item_sub_category"];
        $itemObj["item_image_url"]     = $row["item_image_url_domain"].$row["item_image_url"];
        $itemObj["item_base_quantity"] = $row["item_base_quantity"];
        $itemObj["item_description"]   = $row["item_description"];
        array_push($responseArrOfObjects["item_list"], $itemObj);
    }
    echo json_encode($responseArrOfObjects);
} else {
    $errorResponse              = array();
    $errorResponse["status"]    = "error";
    $errorResponse["error_msg"] = "No Items found";
    $errorResponse["category"]  = $paramCategory;
    $errorResponse["count"]     = $rowsCount;
    echo json_encode($errorResponse);
}

// $responseArrOfObjects["item_list"] = array();
// $itemObj["item_id"]           = "1";
// $itemObj["item_name_english"] = "tamato";
// $itemObj["item_name_hindi"]   = "tamato";
// $itemObj["item_name_marathi"] = "tamato";
// $itemObj["item_sub_category"] = "vegetable";
// $itemObj["item_image_url"]    = "URL";
// $itemObj["item_base_quantity"] = "250 grams";

// array_push($responseArrOfObjects["item_list"], $itemObj);

// $itemObj["item_id"]           = "2";
// $itemObj["item_name_english"] = "apple";
// $itemObj["item_name_hindi"]   = "apple";
// $itemObj["item_name_marathi"] = "apple";
// $itemObj["item_sub_category"] = "fruit";
// $itemObj["item_image_url"]    = "URL";
// $itemObj["item_base_quantity"] = "1 dozen";

// array_push($responseArrOfObjects["item_list"], $itemObj);

// echo json_encode($responseArrOfObjects);

// [
//     {            vegetable/fruit
//         ""
//     }

//     master table

//     1. item_id

//                                                         item_id  category

//     Vegetable_list table
//     item_id
//     item_name
//     item_category: vegetable
//     item_price
//     item_measuring_unit         kg(vegetable) or dozen(fruit)      (FOR fruit)
//     item_image_url

//     ]

//     10 item

//     master_key  tablename                category     vendor_id
//     1           vegetable_table          vegetable        1
//     2           medical_table              medical      2

//     vendor_preference table
//     vendor id and category  item_id

//     1           vegetable   1
//     1           vegetable   2
//     1           vegetable   3

//     2           medical   1
//     2           medical   2
//     2           medical   3
//     2           medical   4
