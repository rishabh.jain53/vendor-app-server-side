<?php

class vendor_db_functions
{
    public function getConnectionObj()
    {
        require_once "../common/common_db_functions.php";
        $cDbFObj = new common_db_functions();
        $connObj = $cDbFObj->getConnectionObj();
        return $connObj;
    }


    /** 
     * Operation on table 'vendor_details'
     */
    /*
    status column values will be
    pending
    active
    blocked
    deleted
     */
    public function isVendorBlocked($connectionObj, $regMobileNo)
    {
        $dbQueryResult = mysqli_query($connectionObj, "SELECT reg_mobile_no from vendor_details
         where reg_mobile_no='" . $regMobileNo . "' AND status = 'blocked'")
        or die(mysqli_error($connectionObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function isMobileNoAlreadyRegistered($connectionObj, $regMobileNo)
    {
        $dbQueryResult = mysqli_query($connectionObj, "SELECT reg_mobile_no from vendor_details
         where reg_mobile_no='" . $regMobileNo . "'")
        or die(mysqli_error($connectionObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function updateVendorDetails($connectionObj, $paramRegMobileNo,
        $paramFcmToken, $paramVendorName, $paramStrVendorCategories, $currentTime) 
    {
        $dbQueryResult = mysqli_query($connectionObj, "UPDATE vendor_details set
        fcm_token ='" . $paramFcmToken . "' , vendor_name ='" . $paramVendorName . "',
         vendor_category = '" . $paramStrVendorCategories . "' ,
         last_updated_on = '" . $currentTime . "' where reg_mobile_no ='" . $paramRegMobileNo . "'")
        or die(mysqli_error($connectionObj));

        return $dbQueryResult;
    }

    public function updateVendorProfileURL($connObj, $serverUrl, $filePath, $vendorCode) 
    {
        $updateQuery = mysqli_query($connObj, "UPDATE vendor_details set
         vendor_image_url_domain='".$serverUrl."', vendor_image_url = '".$filePath."' 
         where vendor_code = '".$vendorCode."'") or die(mysqli_error($connObj));
        return $updateQuery; 
    }

    public function updateVendorProfilePicToDefault($connObj, $vendorCode) 
    {
        $updateProfile = mysqli_query($connObj, "UPDATE vendor_details SET 
        vendor_image_url = DEFAULT(vendor_image_url) where vendor_code = '".$vendorCode."'")
         or die(mysqli_error($connObj));
         return $updateProfile;
    }

    public function updateVendorSettings($connObj, $vendorCode, $vendorName, $nickName,
            $isDeliveryAvailable, $minOrderAmount, $deliveryCharges, 
            $lat, $long, $minWaitingTime, $address, $locality, $city, $state,
            $country, $pinCode, $currentTime, $vendorCategory) 
    {

        $updateSettings = mysqli_query($connObj, "UPDATE vendor_details SET 
        vendor_name = '".$vendorName."', nick_name = '".$nickName."', 
        is_providing_home_delivery = '".$isDeliveryAvailable."', min_order_amount = '".$minOrderAmount."',
        delivery_charges = '".$deliveryCharges."', location_lat = '".$lat."', location_long = '".$long."',
        min_waiting_time_in_minutes = '".$minWaitingTime."', address = '".$address."', locality = '".$locality."',
        city = '".$city."', state = '".$state."', country= '".$country."', pincode = '".$pinCode."', 
        last_updated_on = '".$currentTime."', vendor_category = '".$vendorCategory."' 
        where vendor_code = '".$vendorCode."'")
         or die(mysqli_error($connObj));

         return $updateSettings;

    }

    public function updateVendorFCMtoken($objConn, $vendorCode, $fcmToken, $currentTime){
        $query = mysqli_query($objConn, "UPDATE vendor_details 
        SET fcm_token ='" . $fcmToken . "', last_updated_on = '".$currentTime."' 
         where vendor_code = '".$vendorCode."'" )
         or die(mysqli_error($objConn));
         return $query;
    }
    
    public function updateVendorAvailability($objConn, $vendorCode, $isVendorAvailable, $currentTime){
        $query = mysqli_query($objConn, "UPDATE vendor_details
         set is_taking_order = '".$isVendorAvailable."', last_updated_on = '".$currentTime."'
          where vendor_code ='".$vendorCode."'")
          or die(mysqli_error($objConn));
        return $query;
    }

    public function updateTimeSlotsOfVendor($connObj, $vendorCode, $arrOfSlotsJsonObj){
        $updateQuery = mysqli_query($connObj, "UPDATE vendor_details 
         set time_slots = '".$arrOfSlotsJsonObj."' 
         where vendor_code = '".$vendorCode."'")
         or die(mysqli_error($connObj));
        return $updateQuery;
    }
 
    public function insertVendorDetails($connectionObj, $paramRegMobileNo,
        $paramFcmToken, $vendorCode, $paramVendorName, $currentTime) 
    {
        $dbQueryResult = mysqli_query($connectionObj, "INSERT INTO vendor_details
        (reg_mobile_no, fcm_token, vendor_name, vendor_code, created_on)
        VALUES ('$paramRegMobileNo','$paramFcmToken','$paramVendorName','$vendorCode'
        ,'$currentTime')")
        or die(mysqli_error($connectionObj));

        return $dbQueryResult;
    }

    public function generateVendorCode($connectionObj, $vendorName)
    {
        $vendorInitials = $this->getInitialsOfVendorName($vendorName);
        $vendorInitials = $vendorInitials . "-";

        $isUnique = false;
        $vendorCode;
        while (!$isUnique) {
            $randomNo   = mt_rand(10000, 99999);
            $vendorCode = $vendorInitials . $randomNo;
            $isUnique   = $this->isVendorCodeUnique($connectionObj, $vendorCode);
        }
        return $vendorCode;
    }

    public function getInitialsOfVendorName($vendorName)
    {
        $name = str_replace(" ", "", $vendorName);
        $name = substr($name, 0, 2);
        $name = strtoupper($name);
        $name = trim($name);
        return $name;
    }

    public function isVendorCodeUnique($connectionObj, $vendorCode)
    {
        $dbQueryResult = mysqli_query($connectionObj, "SELECT vendor_code from vendor_details
        where vendor_code='" . $vendorCode . "'")
        or die(mysqli_error($connectionObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return false;
        }
        return true;
    }

    public function isVendorCodePresent($connectionObj, $vendorCode)
    {
        $dbQueryResult = mysqli_query($connectionObj, "SELECT vendor_code from vendor_details
        where vendor_code='" . $vendorCode . "'")
        or die(mysqli_error($connectionObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function getVendorCode($connectionObj, $paramRegMobileNo)
    {
        $dbQueryResult = mysqli_query($connectionObj, "SELECT vendor_code from vendor_details
        where reg_mobile_no='" . $paramRegMobileNo . "'")
        or die(mysqli_error($connectionObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            $parseQueryResult = mysqli_fetch_assoc($dbQueryResult);
            $queryResult      = $parseQueryResult["vendor_code"];
            return $queryResult;
        }
        return null;
    }

    public function getVendorDetails($connObj, $vendorCode) {
        $dbQueryResult = mysqli_query($connObj, "SELECT * from vendor_details
        where vendor_code='" . $vendorCode . "'")
        or die(mysqli_error($connObj));
        
        return $dbQueryResult;
    }

    public function getVendorCategory($connObj, $paramRegMobileNo) {
        $dbQueryResult = mysqli_query($connObj, "SELECT vendor_category from vendor_details
        where reg_mobile_no='" . $paramRegMobileNo . "'")
        or die(mysqli_error($connObj));
        if(mysqli_num_rows($dbQueryResult) > 0) {
            $category = mysqli_fetch_assoc($dbQueryResult);
            return $category["vendor_category"];
        }
        return null;
    }

    public function getVendorCategoryUsingVendorCode($connObj, $vendorCode) {
        $dbQueryResult = mysqli_query($connObj, "SELECT vendor_category from vendor_details
        where vendor_code='" . $vendorCode . "'")
        or die(mysqli_error($connObj));
        if(mysqli_num_rows($dbQueryResult) > 0) {
            $category = mysqli_fetch_assoc($dbQueryResult);
            return $category["vendor_category"];
        }
        return null;
    }

    /** 
     * Operation on table 'vendor_item_preferences'
     */
    public function insertVendorPreferenceItem($objConn, $vendorCode, $itemId, $itemPrice,
     $vendorCategory){
        $insertQuery = mysqli_query($objConn, "INSERT INTO vendor_item_preferences
         (vendor_code, item_id, item_price, vendor_category) 
         VALUES ('$vendorCode', '$itemId', '$itemPrice', '$vendorCategory')") 
         or die(mysqli_error($objConn));
        return $insertQuery;
    }

    public function updateVendorPreferenceItem($objConn, $vendorCode, $itemId, $itemPrice,
     $vendorCategory, $isItemInStock){
        $updateQuery = mysqli_query($objConn, "UPDATE vendor_item_preferences
        SET item_price ='".$itemPrice."' , is_item_in_stock = '".$isItemInStock."' 
        where vendor_code = '".$vendorCode."' 
         AND item_id = '".$itemId."' AND vendor_category = '".$vendorCategory."' ") 
         or die(mysqli_error($objConn));
        
        return $updateQuery;
    }

    public function deleteVendorPreferenceItem($objConn, $vendorCode, $itemId,
     $vendorCategory) {
        $deleteQuery = mysqli_query($objConn, "DELETE from vendor_item_preferences 
         where vendor_code = '".$vendorCode."'
         AND item_id = '".$itemId."' AND vendor_category = '".$vendorCategory."'") 
         or die(mysqli_error($objConn));

        return $deleteQuery;
     }

    /** 
     * JOIN QUERY OPERATIONS
     */
     //RJ-84326
     /**
      * With Tables     vendor_item_preferences, generic_vege_fruit_details
      * values returned onthe basis of category
      */
    public function getVendorPreferenceListOfCategory($objConn, $vendorCode, $vendorCategory){
        $joinQuery = mysqli_query($objConn, "SELECT vip.item_id, vip.item_price, gvfd.item_name_english,
         gvfd.item_name_hindi, gvfd.item_name_marathi, gvfd.item_sub_category, gvfd.item_base_quantity,
         gvfd.item_image_url_domain, gvfd.item_image_url, gvfd.item_description
         from vendor_item_preferences as vip 
         JOIN generic_vege_fruit_details as gvfd on vip.item_id = gvfd.item_id 
         where vip.vendor_code = '".$vendorCode."' AND vip.is_item_in_stock = 1
         AND vip.vendor_category = '".$vendorCategory."' AND gvfd.status = 1 ") 
        or die(mysqli_error($objConn));
        return $joinQuery;
    }

    public function getVendorPreferenceListOfCategoryForVendor($objConn, $vendorCode, $vendorCategory){
        $joinQuery = mysqli_query($objConn, "SELECT vip.item_id, vip.item_price,
         vip.is_item_in_stock, gvfd.item_name_english,
         gvfd.item_name_hindi, gvfd.item_name_marathi, gvfd.item_sub_category, gvfd.item_base_quantity,
         gvfd.item_image_url_domain, gvfd.item_image_url, gvfd.item_description
         from vendor_item_preferences as vip 
         JOIN generic_vege_fruit_details as gvfd on vip.item_id = gvfd.item_id 
         where vip.vendor_code = '".$vendorCode."' AND vip.vendor_category = '".$vendorCategory."' 
         AND gvfd.status = 1 ") 
        or die(mysqli_error($objConn));
        return $joinQuery;
    }

    /**
     * Returns all the pending orders i.e new orders
     * For all vendor vategory 
     * For now there is vegetable_fruit and medical
     */
    public function getNewOrdersOfVendor($objConn, $vendorCode){
        $status = "pending";
        $query = "SELECT *, GROUP_CONCAT(OIL.item_id) as item_ids, 
        GROUP_CONCAT(OIL.item_quantity) as item_quantitys, 
        GROUP_CONCAT(OIL.item_price) as item_prices, 
        GROUP_CONCAT(GVFD.item_name_english) as item_name_englishs, 
        GROUP_CONCAT(GVFD.item_name_hindi) as item_name_hindis, 
        GROUP_CONCAT(GVFD.item_name_marathi) as item_name_marathis, 
        GROUP_CONCAT(GVFD.item_sub_category) as item_sub_categorys, 
        GROUP_CONCAT(GVFD.item_base_quantity) as item_base_quantitys ,
        GROUP_CONCAT(GVFD.item_image_url_domain) as item_image_url_domains,
        GROUP_CONCAT(GVFD.item_image_url) as item_image_urls
        FROM order_details as OD 
        join ordered_item_list as OIL on OD.order_id = OIL.order_id 
        join generic_vege_fruit_details as GVFD on OIL.item_id= GVFD.item_id 
        where OD.vendor_code = '".$vendorCode."' 
        AND OD.order_status = '".$status."' 
        GROUP BY OD.order_id
        ORDER BY OD.order_placed_at DESC"; //"RJ-84326"
        
        $joinQuery = mysqli_query($objConn, $query) 
        or die (mysqli_error($objConn));

        return $joinQuery;
    }

    public function getAcceptedOrderDetailsOfVendor($objConn, $vendorCode,
     $offSet, $offSetLimit){
        require_once './VENDOR_CONSTANTS.php';
        $status  = STR_ACCEPTED;
        $status1 = STR_OUT_FOR_DELIVERY;
        $query = "SELECT *, GROUP_CONCAT(OIL.item_id) as item_ids, 
        GROUP_CONCAT(OIL.item_quantity) as item_quantitys, 
        GROUP_CONCAT(OIL.item_price) as item_prices, 
        GROUP_CONCAT(GVFD.item_name_english) as item_name_englishs, 
        GROUP_CONCAT(GVFD.item_name_hindi) as item_name_hindis, 
        GROUP_CONCAT(GVFD.item_name_marathi) as item_name_marathis, 
        GROUP_CONCAT(GVFD.item_sub_category) as item_sub_categorys, 
        GROUP_CONCAT(GVFD.item_base_quantity) as item_base_quantitys,
        GROUP_CONCAT(GVFD.item_image_url_domain) as item_image_url_domains,
        GROUP_CONCAT(GVFD.item_image_url) as item_image_urls 
        FROM order_details as OD 
        join ordered_item_list as OIL on OD.order_id = OIL.order_id 
        join generic_vege_fruit_details as GVFD on OIL.item_id= GVFD.item_id 
        where OD.vendor_code = '".$vendorCode."' 
        AND OD.order_status IN ('$status','$status1')
        GROUP BY OD.order_id
        ORDER BY OD.order_placed_at DESC limit ".$offSet.",".$offSetLimit."";
        
        $joinQuery = mysqli_query($objConn, $query) 
        or die (mysqli_error($objConn));

        return $joinQuery;
    }

    public function getCompletedOrdersDetailsOfVendor($objConn, $vendorCode,
     $offSet, $offSetLimit){
        require_once './VENDOR_CONSTANTS.php';
        $status  = STR_COMPLETED;
        $status1 = STR_REJECTED;
        $status2 = STR_CANCELLED;
        $query = "SELECT *, GROUP_CONCAT(OIL.item_id) as item_ids, 
        GROUP_CONCAT(OIL.item_quantity) as item_quantitys, 
        GROUP_CONCAT(OIL.item_price) as item_prices, 
        GROUP_CONCAT(GVFD.item_name_english) as item_name_englishs, 
        GROUP_CONCAT(GVFD.item_name_hindi) as item_name_hindis, 
        GROUP_CONCAT(GVFD.item_name_marathi) as item_name_marathis, 
        GROUP_CONCAT(GVFD.item_sub_category) as item_sub_categorys, 
        GROUP_CONCAT(GVFD.item_base_quantity) as item_base_quantitys,
        GROUP_CONCAT(GVFD.item_image_url_domain) as item_image_url_domains,
        GROUP_CONCAT(GVFD.item_image_url) as item_image_urls 
        FROM order_details as OD 
        join ordered_item_list as OIL on OD.order_id = OIL.order_id 
        join generic_vege_fruit_details as GVFD on OIL.item_id= GVFD.item_id 
        where OD.vendor_code = '".$vendorCode."' 
        AND OD.order_status IN ('$status', '$status1', '$status2') 
        GROUP BY OD.order_id
        ORDER BY OD.order_placed_at DESC limit ".$offSet.",".$offSetLimit." ";
        
        $joinQuery = mysqli_query($objConn, $query) 
        or die (mysqli_error($objConn));

        return $joinQuery;
    }

    /**
     * Operations on order_details table
     */
     public function isOrderIdAssociatedWithSameVendorCode($connObj, $vendorCode, $orderId){
        $dbQueryResult = mysqli_query($connObj, "SELECT vendor_code from order_details
        where vendor_code='" . $vendorCode . "' AND order_id = '".$orderId."' ")
        or die(mysqli_error($connObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function updateOrderStatus($connObj, $orderId, $orderStatus, $currentTime,
    $cancellationReason = null, $rejectionReason = null) {
       $query = mysqli_query($connObj, "UPDATE order_details 
       SET order_status = '".$orderStatus."', 
        customer_cancellation_reason = '".$cancellationReason."',
        vendor_cancellation_reason = '".$rejectionReason."',
        order_last_updated = '".$currentTime."'
        WHERE order_id = '".$orderId."' ")
        or die(mysqli_error($connObj));

       return $query;
   }

   public function isOrderInParticularStatus($connObj, $orderId, $arrOrderStatus) {

        $arrStatus = implode('","',$arrOrderStatus);
        $dbQueryResult = mysqli_query($connObj, "SELECT order_status from order_details
        where order_id = '".$orderId."' AND order_status IN (\"$arrStatus\") ")
        or die(mysqli_error($connObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
   }

   public function isOrderNotInParticularStatus($connObj, $orderId, $arrOrderStatus) {
        $arrStatus = implode('","',$arrOrderStatus);
        $dbQueryResult = mysqli_query($connObj, "SELECT order_status from order_details
        where order_id = '".$orderId."' AND order_status NOT IN (\"$arrStatus\") ")
        or die(mysqli_error($connObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function getCustomerMobileNoUsingOrderId($connObj, $orderId){
        $dbquery = mysqli_query($connObj, "SELECT reg_mob_no from order_details
        where order_id = '".$orderId."' ")
        or die(mysqli_error($connObj));
        if(mysqli_num_rows($dbquery) > 0) {
            $customerMobileNo = mysqli_fetch_assoc($dbquery);
            return $customerMobileNo["reg_mob_no"];
        }  
        return null;
    }
  
    /**
     * CRUD Operation on table order_status_details
     */
    public function insertOrderNewStatusInOrderStatusDetails($connObj, $orderId, $customerMobileNo,
     $vendorCode, $vendorCategory, $hasOptForHomeDelivery,
     $orderTotalAmount, $customerName, $minWaitingTime, 
     $vendorName, $vendorMobNumber, $orderStatus, $cancellationReason = null, $rejectionReason = null,
     $lat = null, $long = null, $address = null,
     $locality = null, $city = null, $state = null, $country = null, $pincode = null,
     $deliveryCharges = 0){

        require_once './utils.php';
        $currentTime = utilsGetDateAndTimeInIST();

        $dbQuery = mysqli_query($connObj, "INSERT INTO order_status_details
        (order_id, reg_mob_no, vendor_code, vendor_category, has_opt_for_home_delivery, total_amount,
        delivery_lat, delivery_long, address, locality, city, state, country, pin_code, 
        customer_name, min_waiting_time_in_minutes, vendor_name, 
        vendor_number, delivery_charges, order_status, customer_cancellation_reason,
        vendor_cancellation_reason, order_placed_at)
        VALUES ('$orderId','$customerMobileNo','$vendorCode', '$vendorCategory',
         '$hasOptForHomeDelivery','$orderTotalAmount','$lat', '$long', '$address',
         '$locality', '$city', '$state', '$country', '$pincode','$customerName',
         '$minWaitingTime', '$vendorName', '$vendorMobNumber', 
         '$deliveryCharges', '$orderStatus', '$cancellationReason', '$rejectionReason', 
         '$currentTime') ") 
         or die(mysqli_error($connObj));

        return $dbQuery;
    }
    
    public function copyOrderRowAndInsertInOrderStatusDetails($connObj, $orderId) {

        $dbQuery = mysqli_query($connObj, "INSERT INTO order_status_details
         (order_id, reg_mob_no, vendor_code, vendor_category, has_opt_for_home_delivery,
         total_amount, delivery_lat, delivery_long, address, locality,
         city, state, country, pin_code, 
         customer_name, min_waiting_time_in_minutes, vendor_name, 
         vendor_number, delivery_charges, order_status, customer_cancellation_reason,
         vendor_cancellation_reason, order_placed_at) SELECT order_id, reg_mob_no, vendor_code,
         vendor_category, has_opt_for_home_delivery,
         total_amount, delivery_lat, delivery_long, address, locality,
         city, state, country, pin_code, 
         customer_name, min_waiting_time_in_minutes, vendor_name, 
         vendor_number, delivery_charges, order_status, customer_cancellation_reason,
         vendor_cancellation_reason, order_last_updated
         FROM order_details WHERE order_id = '".$orderId."' ") 
         or die(mysqli_error($connObj));

         return $dbQuery;
    }

    /**
     * Operation on table order_status_details
     */

    public function getCustomerFcmToken($connObj, $customerMobileNo){
        $dbquery = mysqli_query($connObj, "SELECT fcm_token 
        from customer_details
        where reg_mobile_no = '".$customerMobileNo."' ")
         or die (mysqli_error($connObj));
        if(mysqli_num_rows($dbquery) > 0) {
            $token = mysqli_fetch_assoc($dbquery);
            return $token["fcm_token"];
        }  
        return null;
    }


    /** 
     * New for multiple categories in one table itself
     * Operation on table 'generic_vege_fruit_details'
     */

    public function insertIntoGenericItemsTable($connectionObj,
    $paramEnglishName, $paramHindiName,
    $paramMarathiName, $paramSubCategory, $paramBaseQuantity,
    $serverUrl, $paramImageUrl, $category, $paramDescription = null){
       $insertQuery = mysqli_query($connectionObj, "INSERT INTO generic_vege_fruit_details
       (item_name_english, item_name_hindi, item_name_marathi, item_sub_category,
        item_image_url, item_base_quantity, item_image_url_domain,
        item_description, category)
        VALUES ('$paramEnglishName', '$paramHindiName', '$paramMarathiName',
        '$paramSubCategory', '$paramImageUrl', '$paramBaseQuantity',
        '$serverUrl', '$paramDescription', '$category' )") 
        or die(mysqli_error($connectionObj));
       return $insertQuery; 
   }

   public function getGenericListOfCategory($connectionObj, $vendorCategory){
    $dbQuery = mysqli_query($connectionObj, "SELECT * from generic_vege_fruit_details
     where category = '".$vendorCategory."' AND status = 1") 
     or die(mysqli_error($connectionObj));
    return $dbQuery;
}
}
