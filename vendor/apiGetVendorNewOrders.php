<?php
//DONE
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramVendorCode = $requestParams->vendor_code;

$objResponse = array();
if ($paramVendorCode == null) {
    //error
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 1 required";
} else {
    require_once './VENDOR_CONSTANTS.php';
    require_once './utils.php';
    require_once './vendor_db_functions.php';
    $objOfVendorDbFunc = new vendor_db_functions();
    $objConn           = $objOfVendorDbFunc->getConnectionObj();

    $doesVendorCodeExists = $objOfVendorDbFunc->isVendorCodePresent($objConn, $paramVendorCode);
    if ($doesVendorCodeExists) {

        $newOrders = $objOfVendorDbFunc->getNewOrdersOfVendor($objConn, $paramVendorCode);

        $objResponse["status"] = "success";

        if (mysqli_num_rows($newOrders) > 0) {
            $formattedJson = utilsGetOrdersInJSONFormatNew($newOrders);
            $objResponse["count"] = count($formattedJson);
            $objResponse["orders"] = $formattedJson;
        } else {
            $objResponse["count"] = 0;
            $objResponse["orders"] = "No orders found";
        }

    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "200";
        $objResponse["error_msg"]  = "invalid vendor code";
    }

}
echo json_encode($objResponse);


/*
{
  "status": "success",
  "count": 3,
  "orders": [
    {
      "order_id": "1",
      "has_opt_for_home_delivery": false,
      "total_amount": "200.00",
      "delivery_charges": null,
      "customer_cancellation_reason": null,
      "vendor_cancellation_reason": null,
      "rating": null,
      "remark": null,
      "order_placed_at": "2020-05-24 07:47:21",
      "order_status": "pending",
      "item_list": [
        {
          "item_id": "1",
          "item_quantity": "20",
          "item_price": "4.00",
          "item_name_english": "tamato",
          "item_name_hindi": "\u091f\u092e\u093e\u091f\u0930",
          "item_name_marathi": "\u091f\u094b\u092e\u0945\u091f\u094b",
          "item_sub_category": "vegetable",
          "item_base_quantity": "200 grams"
        },
        {
          "item_id": "6",
          "item_quantity": "30",
          "item_price": "2.00",
          "item_name_english": "Taaamato yellow",
          "item_name_hindi": "\u091f\u092e\u093e\u091f\u0930 yellow",
          "item_name_marathi": "\u091f\u094b\u092e\u0945\u091f\u094b yellow",
          "item_sub_category": "vegetable",
          "item_base_quantity": "300 grams"
        },
        {
          "item_id": "4",
          "item_quantity": "20",
          "item_price": "1.00",
          "item_name_english": "Tamato yellow",
          "item_name_hindi": "\u091f\u092e\u093e\u091f\u0930 yellow",
          "item_name_marathi": "\u091f\u094b\u092e\u0945\u091f\u094b yellow",
          "item_sub_category": "vegetable",
          "item_base_quantity": "500 grams"
        }
      ]
    },
    {
      "order_id": "2",
      "has_opt_for_home_delivery": false,
      "total_amount": "200.00",
      "delivery_charges": null,
      "customer_cancellation_reason": null,
      "vendor_cancellation_reason": null,
      "rating": null,
      "remark": null,
      "order_placed_at": "2020-05-24 07:55:08",
      "order_status": "pending",
      "item_list": [
        {
          "item_id": "5",
          "item_quantity": "4",
          "item_price": "200.00",
          "item_name_english": "Taamato yellow",
          "item_name_hindi": "\u091f\u092e\u093e\u091f\u0930 yellow",
          "item_name_marathi": "\u091f\u094b\u092e\u0945\u091f\u094b yellow",
          "item_sub_category": "vegetable",
          "item_base_quantity": "700 grams"
        },
        {
          "item_id": "1",
          "item_quantity": "2",
          "item_price": "301.00",
          "item_name_english": "tamato",
          "item_name_hindi": "\u091f\u092e\u093e\u091f\u0930",
          "item_name_marathi": "\u091f\u094b\u092e\u0945\u091f\u094b",
          "item_sub_category": "vegetable",
          "item_base_quantity": "200 grams"
        },
        {
          "item_id": "4",
          "item_quantity": "1",
          "item_price": "220.00",
          "item_name_english": "Tamato yellow",
          "item_name_hindi": "\u091f\u092e\u093e\u091f\u0930 yellow",
          "item_name_marathi": "\u091f\u094b\u092e\u0945\u091f\u094b yellow",
          "item_sub_category": "vegetable",
          "item_base_quantity": "500 grams"
        }
      ]
    },
    {
      "order_id": "3",
      "has_opt_for_home_delivery": true,
      "delivery_lat": "72.121212",
      "delivery_long": "138.2111",
      "address": "address",
      "locality": "locality",
      "city": "Rajkot",
      "state": "Gujarat",
      "country": "India",
      "pin_code": "1111104",
      "total_amount": "200.00",
      "delivery_charges": null,
      "customer_cancellation_reason": null,
      "vendor_cancellation_reason": null,
      "rating": null,
      "remark": null,
      "order_placed_at": "2020-05-24 08:02:37",
      "order_status": "pending",
      "item_list": [
        {
          "item_id": "6",
          "item_quantity": "4",
          "item_price": "200.00",
          "item_name_english": "Taaamato yellow",
          "item_name_hindi": "\u091f\u092e\u093e\u091f\u0930 yellow",
          "item_name_marathi": "\u091f\u094b\u092e\u0945\u091f\u094b yellow",
          "item_sub_category": "vegetable",
          "item_base_quantity": "300 grams"
        },
        {
          "item_id": "4",
          "item_quantity": "2",
          "item_price": "301.00",
          "item_name_english": "Tamato yellow",
          "item_name_hindi": "\u091f\u092e\u093e\u091f\u0930 yellow",
          "item_name_marathi": "\u091f\u094b\u092e\u0945\u091f\u094b yellow",
          "item_sub_category": "vegetable",
          "item_base_quantity": "500 grams"
        },
        {
          "item_id": "1",
          "item_quantity": "1",
          "item_price": "220.00",
          "item_name_english": "tamato",
          "item_name_hindi": "\u091f\u092e\u093e\u091f\u0930",
          "item_name_marathi": "\u091f\u094b\u092e\u0945\u091f\u094b",
          "item_sub_category": "vegetable",
          "item_base_quantity": "200 grams"
        }
      ]
    }
  ]
}

*/

?>
