<?php
//Tb tested
$jsonObj                  = file_get_contents('php://input');
$requestParams            = json_decode($jsonObj);
$paramRegMobileNo         = $requestParams->reg_mobile_no;
$paramFcmToken            = $requestParams->fcm_token;
$paramVendorName          = $requestParams->vendor_name;
//Rishabh: Supporting only one category per vendor
//Moved this key in apiUpdateVendorSettings api (26th june 2020) only for new vendor
//$paramStrVendorCategories = $requestParams->vendor_category;

$responseArray = array();
if ($paramRegMobileNo==null || $paramVendorName==null) {
    //error
    $responseArray["status"] = "error";
    $responseArray["error_code"] = "1";   
    $responseArray["error_msg"] = "Invalid request parameters 2 required";    
} else {
    if($paramFcmToken == null){
        $paramFcmToken = "dummy";
    }
    require_once './VENDOR_CONSTANTS.php';
    include_once "./utils.php";
    $currentTime              = utilsGetDateAndTimeInIST();
    
    include_once "./vendor_db_functions.php";
    $vendorDBObj = new vendor_db_functions();
    $connObj = $vendorDBObj->getConnectionObj();
    
    $isVendorBlocked = $vendorDBObj->isVendorBlocked($connObj, $paramRegMobileNo);
    if($isVendorBlocked){
        //error
        $responseArray["status"] = "error";   
        $responseArray["error_code"] = "500";   
        $responseArray["error_msg"] = "Dear Saler, your number is blocked due to some reason. Please contact the authorized person.";  
    } else {
        $isAlreadyRegistered = $vendorDBObj->isMobileNoAlreadyRegistered($connObj, $paramRegMobileNo);
        if ($isAlreadyRegistered) {
            $strVendorCategory = $vendorDBObj->getVendorCategory($connObj, $paramRegMobileNo);

            if($strVendorCategory == null) {
                //error
                //Special handling for this case on native side
                $vendorCode = $vendorDBObj->getVendorCode($connObj, $paramRegMobileNo);
                $responseArray["vendor_code"] = $vendorCode;   
                $responseArray["status"] = "error";   
                $responseArray["error_code"] = "510";   
                $responseArray["error_msg"] = "Vendor category not found";  
                echo json_encode($responseArray);
                exit();
            }
            
            $strVendorCategory = strtolower($strVendorCategory);
            $isQueryExecuted = $vendorDBObj->updateVendorDetails($connObj, $paramRegMobileNo,
            $paramFcmToken, $paramVendorName, $strVendorCategory, $currentTime);

            if ($isQueryExecuted) {
                //success
                $responseArray["status"] = "success";
                $responseArray["operation"] = "update";
                $vendorCode = $vendorDBObj->getVendorCode($connObj, $paramRegMobileNo);
                if ($vendorCode!=null) {
                    $responseArray["vendor_code"] = $vendorCode;
                    
                    //Vendor details
                    $getVendorDetails = $vendorDBObj->getVendorDetails($connObj, $vendorCode);
                    if(mysqli_num_rows($getVendorDetails) > 0) {
                        $responseArray["existing_details"] = parseVendorDetailsQueryResults($getVendorDetails);
                    }

                    //Vendor preference list
                    $getVendorPreferenceList = $vendorDBObj->getVendorPreferenceListOfCategoryForVendor(
                        $connObj,
                        $vendorCode,
                        $strVendorCategory);
                                        
                    $responseArray["item_list"] = array();
                    if(mysqli_num_rows($getVendorPreferenceList)>0) {
                        $responseArray["item_list"] = parseItemListQueryResult($getVendorPreferenceList);
                    }

                } else {
                    //NOTE: SHOULD NEVER COME IN ELSE PART
                    $responseArray["error_code"] = "1000"; 
                    $responseArray["error_msg"] = "Unable to generate vendorCode"; 
                }
            } else {
                //error
                $responseArray["status"] = "error";   
                $responseArray["error_code"] = "102";   
                $responseArray["error_msg"] = "Unable to update the details";         
            }
        } else {
            $vendorCode = $vendorDBObj->generateVendorCode($connObj, $paramVendorName);

            //insert
            $isQueryExecuted = $vendorDBObj->insertVendorDetails($connObj, $paramRegMobileNo,
            $paramFcmToken, $vendorCode, $paramVendorName, $currentTime);  

            if ($isQueryExecuted) {
                //success
                $responseArray["status"] = "success";
                $responseArray["operation"] = "insert";
                $responseArray["vendor_code"] = $vendorCode;
            } else {
                //error
                $responseArray["status"] = "error";   
                $responseArray["error_code"] = "101";   
                $responseArray["error_msg"] = "Unable to insert the details";         
            }
        }
    }

    
}
echo json_encode($responseArray);


function parseVendorDetailsQueryResults($getVendorDetailsResultQuery) {
    $row = mysqli_fetch_assoc($getVendorDetailsResultQuery);
    $details["reg_mobile_no"] = $row["reg_mobile_no"];
    $details["alt_mobile_no"] = $row["alt_mobile_no"];
    $details["vendor_name"] = $row["vendor_name"];
    $details["nick_name"] = $row["nick_name"];
    $details["address"] = $row["address"];
    $details["latitude"] = $row["location_lat"];
    $details["longitude"] = $row["location_long"];
    $details["vendor_code"] = $row["vendor_code"];
    $details["is_taking_order"] = $row["is_taking_order"];
    $details["is_providing_home_delivery"] = $row["is_providing_home_delivery"];
    $details["vendor_rating"] = $row["vendor_rating"];
    $details["min_waiting_time_in_minutes"] = $row["min_waiting_time_in_minutes"];
    $details["status"] = $row["status"];
    $details["created_on"] = $row["created_on"];
    $details["last_updated_on"] = $row["last_updated_on"];
    $details["vendor_category"] = $row["vendor_category"];
    $details["vendor_image"] = $row["vendor_image_url_domain"] . $row["vendor_image_url"];
    $details["min_order_amount"] = $row["min_order_amount"];
    $details["delivery_charges"] = $row["delivery_charges"];
    $details["locality"] = $row["locality"];
    $details["city"] = $row["city"];
    $details["state"] = $row["state"];
    $details["country"] = $row["country"];
    $details["pincode"] = $row["pincode"];
    $details["time_slots"] = $row["time_slots"];

    return $details;
}

function parseItemListQueryResult ($itemList) {
    $arrItemList = array();
    while($row = mysqli_fetch_assoc($itemList)){
        $itemObj = array();
        $itemObj["item_id"]            = $row["item_id"];
        $itemObj["item_price"]         = $row["item_price"];
        $itemObj["item_name_english"]  = $row["item_name_english"];
        $itemObj["item_name_hindi"]    = $row["item_name_hindi"];
        $itemObj["item_name_marathi"]  = $row["item_name_marathi"];
        $itemObj["item_sub_category"]  = $row["item_sub_category"];
        $itemObj["item_base_quantity"] = $row["item_base_quantity"];
        $itemObj["item_image_url"]     = $row["item_image_url_domain"] . $row["item_image_url"];
        $itemObj["is_available"]       = ($row["is_item_in_stock"]) ? "true" : "false";
        $itemObj["item_description"]   = $row["item_description"];
        array_push($arrItemList, $itemObj);
    }
    return $arrItemList;
}