<?php
$jsonObj       = file_get_contents('php://input');
$requestParams = json_decode($jsonObj);
$paramFCMToken = $requestParams->fcm_token;    
$paramVendorCode = $requestParams->vendor_code;

$objResponse = array();

if ($paramVendorCode == null || $paramFCMToken == null) {
    //error
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 2 required";
} else {
    require_once './vendor_db_functions.php';
    $vendorDBObj = new vendor_db_functions();
    $objConn = $vendorDBObj->getConnectionObj();

    $doesVendorCodeExists = $vendorDBObj->isVendorCodePresent($objConn, $paramVendorCode);
    if($doesVendorCodeExists){
        require_once './utils.php';
        $currentTime              = utilsGetDateAndTimeInIST();
        $updateFCM = $vendorDBObj->updateVendorFCMtoken($objConn, $paramVendorCode,
         $paramFCMToken, $currentTime);
        if($updateFCM){
            $objResponse["status"] = "success";
        } else {
            $objResponse["status"] = "error";
            $objResponse["error_code"] = "103";   
            $objResponse["error_msg"] = "unable to update";  
        }
    } else {
        $objResponse["status"] = "error";
        $objResponse["error_code"] = "200";   
        $objResponse["error_msg"] = "invalid vendor code";   
    }

}

echo json_encode($objResponse);

?>