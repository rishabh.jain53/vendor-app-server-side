<?php
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramVendorCode = $requestParams->vendor_code;
$paramOrderId = $requestParams->order_id;

$objResponse = array();

if ($paramVendorCode == null || $paramOrderId == null ) {
    //error
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 2 required";
} else {
    require_once './VENDOR_CONSTANTS.php';
    require_once './utils.php';
    require_once './vendor_db_functions.php';
    $objOfVendorDbFunc = new vendor_db_functions();
    $objConn           = $objOfVendorDbFunc->getConnectionObj();

    $currentTime = utilsGetDateAndTimeInIST();
    $doesOrderIdAndVendorCodeMatch = $objOfVendorDbFunc->isOrderIdAssociatedWithSameVendorCode($objConn,
     $paramVendorCode, $paramOrderId);

    if ($doesOrderIdAndVendorCodeMatch) {
        $arrOfOrderStatus         = array(STR_ACCEPTED, STR_OUT_FOR_DELIVERY);
        $isOrderInParticularState = $objOfVendorDbFunc->isOrderInParticularStatus($objConn,
         $paramOrderId, $arrOfOrderStatus);
        if ($isOrderInParticularState) {
         $orderStatus       = STR_COMPLETED;
         $updateOrderStatus = $objOfVendorDbFunc->updateOrderStatus($objConn,
          $paramOrderId, $orderStatus, $currentTime);

          if($updateOrderStatus) {
            $objResponse["status"]                    = "success";
            $objResponse["order_status_changed_to"]   =  $orderStatus;

            $insertInTimeLineTable = $objOfVendorDbFunc->copyOrderRowAndInsertInOrderStatusDetails(
                $objConn, 
                $paramOrderId);

            $customerMobileNo = $objOfVendorDbFunc->getCustomerMobileNoUsingOrderId($objConn, $paramOrderId);
            if($customerMobileNo != null) {
                $fcmToken = $objOfVendorDbFunc->getCustomerFcmToken($objConn, $customerMobileNo);
                if($fcmToken != null) {
                    utilsSendFCMMessageSingle($fcmToken, "Happy shopping! Hope you have received your packet.", 
                            "Update for Order No. " . $paramOrderId, FIREBASE_KEY_CUSTOMER, $orderStatus, STR_ACCEPTED);
                }
            }
            
          } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "1000";
            $objResponse["error_msg"]  =  "Shouldn't come here! Unable to update order status";
          }
          
        } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "504";
            $objResponse["error_msg"]  = "Order is not in accepted or out for delivery state";
        }

    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "502";
        $objResponse["error_msg"]  = "This order id doesn't belong to this vendor";
    }
}
echo json_encode($objResponse);
?>