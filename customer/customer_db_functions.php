<?php


class customer_db_functions {

    public function getConnectionObj()
    {
        require_once "../common/common_db_functions.php";
        $cDbFObj = new common_db_functions();
        $connObj = $cDbFObj->getConnectionObj();
        return $connObj;
    }


    /**
     * Operation on table customer_details
     */
    
     public function isMobileNoAlreadyRegistered($connObj, $mobileNo){
        $isRegistered = mysqli_query($connObj, "SELECT reg_mobile_no from customer_details
         WHERE reg_mobile_no = '".$mobileNo."'") 
        or die(mysqli_error($connObj));
        if (mysqli_num_rows($isRegistered) > 0) {
            return true;
        }
        return false;
     }

     public function isMobileNoBlocked($connObj, $mobileNo){
        $isBlocked = mysqli_query($connObj, "SELECT reg_mobile_no from customer_details
         WHERE reg_mobile_no = '".$mobileNo."' AND is_blocked = 1 ") 
        or die(mysqli_error($connObj));
        if (mysqli_num_rows($isBlocked) > 0) {
            return true;
        }
        return false;
     }

     public function updateCustomerFcmTokenAndCustomerName($connObj, $mobileNo,
      $fcmToken, $customerName, $currentTime){
        $update = mysqli_query($connObj, "UPDATE customer_details 
        SET fcm_token = '".$fcmToken."' , cust_name = '".$customerName."', 
        last_updated_on = '".$currentTime."'
         WHERE reg_mobile_no = '".$mobileNo."'") 
       or die(mysqli_error($connObj));
       return $update;
     }

     public function updateCustomerFcmToken($objConn, $mobileNo, $fcmToken, $currentTime){
        $query = mysqli_query($objConn, "UPDATE customer_details 
        SET fcm_token = '".$fcmToken."' , last_updated_on = '".$currentTime."' 
         WHERE reg_mobile_no = '".$mobileNo."'")
         or die(mysqli_error($objConn));
         return $query;
    }

    public function updateCustomerDetails($objConn, $mobileNo,
        $altMobileNo, $customerName, $address, $locality, $city,
        $state, $country, $pincode, $currentTime) {

        $query = mysqli_query($objConn, "UPDATE customer_details 
         SET alt_mobile_no = '".$altMobileNo."' , cust_name = '".$customerName."', 
         address = '".$address."', locality = '".$locality."', city = '".$city."', 
         state = '".$state."',  country = '".$country."',  pincode = '".$pincode."', 
         last_updated_on = '".$currentTime."'
         WHERE reg_mobile_no = '".$mobileNo."'")
         or die(mysqli_error($objConn));

         return $query;
    }

     public function insertRegisterCustomer($connObj, $mobileNo, $fcmToken,
      $customerName, $currentTime){
        $insert = mysqli_query($connObj, "INSERT INTO customer_details 
         (reg_mobile_no, fcm_token, cust_name, created_on) 
         VALUES ('$mobileNo','$fcmToken','$customerName', '$currentTime')") 
         or die(mysqli_error($connObj)); 
         return $insert;
     }

     public function getCustomerDetails($connObj, $mobileNo){
         $getDetails = mysqli_query($connObj, "SELECT * FROM customer_details
           WHERE reg_mobile_no = '".$mobileNo."' ") 
           or die(mysqli_error($connObj));
          
          return $getDetails;
     }

     /**
     * Select Operation on table vendor_details
     */
     public function getNearByVendorList($objConn, $lat, $long){
         $distanceFormula = "( 6371 * acos( cos( radians(".$lat.") ) * cos( radians( location_lat ) )
          * cos( radians( location_long ) - radians(".$long.") ) 
          + sin( radians(".$lat.") ) * sin( radians( location_lat ) ) ) ) 
          AS distance";
         $query = mysqli_query($objConn, "SELECT * , ".$distanceFormula."
         FROM vendor_details HAVING distance < 5 AND is_taking_order = 1 
         AND status = 'active' OR isGloballyAvailable = 1 ORDER BY distance LIMIT 0 , 20") 
         or die(mysqli_error($objConn));
         return $query;
     }

     public function isVendorCodePresent($connectionObj, $vendorCode)
    {
        $dbQueryResult = mysqli_query($connectionObj, "SELECT vendor_code from vendor_details
        where vendor_code='" . $vendorCode . "'")
        or die(mysqli_error($connectionObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function isVendorCurrentlyTakingOrders($connectionObj, $vendorCode){
        $dbQueryResult = mysqli_query($connectionObj, "SELECT vendor_code from vendor_details
        where is_taking_order = 1 AND vendor_code = '".$vendorCode."' ")
        or die(mysqli_error($connectionObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function isVendorProvidingHomeDelivery($connObj, $vendorCode){
        $dbQueryResult = mysqli_query($connObj, "SELECT vendor_code from vendor_details
        where is_providing_home_delivery = 1 AND vendor_code = '".$vendorCode."' ")
        or die(mysqli_error($connObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function getVendorFcmToken($connObj, $vendorCode){
        $dbquery = mysqli_query($connObj, "SELECT fcm_token 
        from vendor_details
        where vendor_code = '".$vendorCode."' ")
         or die (mysqli_error($connObj));
        if(mysqli_num_rows($dbquery) > 0) {
            $token = mysqli_fetch_assoc($dbquery);
            return $token["fcm_token"];
        }  
        return null;
    }

    public function getVendorRatings($objConn, $vendorCode){
        $dbquery = mysqli_query($objConn, "SELECT vendor_rating 
        from vendor_details
        where vendor_code = '".$vendorCode."' ")
         or die (mysqli_error($objConn));
        if(mysqli_num_rows($dbquery) > 0) {
            $rating = mysqli_fetch_assoc($dbquery);
            return $rating["vendor_rating"];
        }  
        return 0;
    }

    public function updateVendorRating($objConn, 
     $vendorCode, $newRating){
        $query = mysqli_query($objConn, "UPDATE vendor_details 
        SET vendor_rating = '".$newRating."'
         WHERE vendor_code = '".$vendorCode."' ")
         or die(mysqli_error($objConn));
 
        return $query;
    }

    /**
     * CRUD Operation on table order_details
     */
    public function getLastPlacedOrderID($connObj, $customerMobile,
     $vendorCode, $vendorCategory){
        $dbQuery = mysqli_query($connObj, "SELECT order_id from order_details
        where reg_mob_no='".$customerMobile."' AND vendor_code='".$vendorCode."'
         AND vendor_category ='".$vendorCategory."' order by order_id DESC limit 0,1 ") 
        or die(mysqli_error($connObj));
        $row = mysqli_fetch_assoc($dbQuery);

        return $row["order_id"];
    }

    public function insertPlaceOrder($connObj, $customerMobileNo,
     $vendorCode, $vendorCategory, $hasOptForHomeDelivery,
     $orderTotalAmount, $customerName, $minWaitingTime, $vendorName, $vendorMobNumber,
     $lat = null, $long = null, $address = null,
     $locality = null, $city = null, $state = null, $country = null, $pincode = null,
     $deliveryCharges = 0){
        require_once './utils.php';
        $currentTime = utilsGetDateAndTimeInIST();

        $dbQuery = mysqli_query($connObj,"INSERT INTO order_details
        (reg_mob_no, vendor_code, vendor_category, has_opt_for_home_delivery, total_amount,
        delivery_lat, delivery_long, address, locality, city, state, country, pin_code, 
        customer_name, min_waiting_time_in_minutes, vendor_name, 
        vendor_number, delivery_charges, order_placed_at)
        VALUES ('$customerMobileNo','$vendorCode', '$vendorCategory',
         '$hasOptForHomeDelivery','$orderTotalAmount','$lat', '$long', '$address',
         '$locality', '$city', '$state', '$country', '$pincode','$customerName',
         '$minWaitingTime', '$vendorName', '$vendorMobNumber', '$deliveryCharges',
         '$currentTime')") 
         or die(mysqli_error($connObj));

        return $dbQuery;
    }

    public function isOrderIdAssociatedWithSameMobileNo($connObj, $mobileNo, $orderId){
        $dbQueryResult = mysqli_query($connObj, "SELECT reg_mob_no from order_details
        where reg_mob_no='" . $mobileNo . "' AND order_id = '".$orderId."' ")
        or die(mysqli_error($connObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function isOrderInParticularStatus($connObj, $orderId, $orderStatus) {
        $dbQueryResult = mysqli_query($connObj, "SELECT order_id from order_details
        where order_id='" . $orderId . "' AND order_status = '".$orderStatus."' ")
        or die(mysqli_error($connObj));
        if (mysqli_num_rows($dbQueryResult) > 0) {
            return true;
        }
        return false;
    }

    public function updateOrderStatus($connObj, $orderId, $orderStatus, $currentTime,
    $cancellationReason = null, $rejectionReason = null) {
       $query = mysqli_query($connObj, "UPDATE order_details 
       SET order_status = '".$orderStatus."', 
        customer_cancellation_reason = '".$cancellationReason."',
        vendor_cancellation_reason = '".$rejectionReason."',
        order_last_updated = '".$currentTime."'
        WHERE order_id = '".$orderId."' ")
        or die(mysqli_error($connObj));

       return $query;
   }

   public function setOrderFeedbackRating($objConn, $orderId, 
    $averageRating, $feedbackInDetail, $remarkIfAny, $currentTime) {
        $query = mysqli_query($objConn, "UPDATE order_details 
        SET rating = '".$averageRating."', 
         feedback_in_detail = '".$feedbackInDetail."',
         remark = '".$remarkIfAny."',
         feedback_given_on = '".$currentTime."'
         WHERE order_id = '".$orderId."' ")
         or die(mysqli_error($objConn));
 
        return $query;
   }

    /**
     * CRUD Operation on table order_status_details
     */
    public function insertOrderNewStatusInOrderStatusDetails($connObj, $orderId, $customerMobileNo,
     $vendorCode, $vendorCategory, $hasOptForHomeDelivery,
     $orderTotalAmount, $customerName, $minWaitingTime, $vendorName, $vendorMobNumber, $orderStatus, 
     $cancellationReason = null, $lat = null, $long = null, $address = null,
     $locality = null, $city = null, $state = null, $country = null, $pincode = null,
     $deliveryCharges = 0){
        require_once './utils.php';
        $currentTime = utilsGetDateAndTimeInIST();

        $dbQuery = mysqli_query($connObj,"INSERT INTO order_status_details
        (order_id, reg_mob_no, vendor_code, vendor_category, has_opt_for_home_delivery, total_amount,
        delivery_lat, delivery_long, address, locality, city, state, country, pin_code, 
        customer_name, min_waiting_time_in_minutes, vendor_name, 
        vendor_number, delivery_charges, order_status, customer_cancellation_reason, 
        order_placed_at)
        VALUES ('$orderId','$customerMobileNo','$vendorCode', '$vendorCategory',
         '$hasOptForHomeDelivery','$orderTotalAmount','$lat', '$long', '$address',
         '$locality', '$city', '$state', '$country', '$pincode','$customerName',
         '$minWaitingTime', '$vendorName', '$vendorMobNumber',
         '$deliveryCharges', '$orderStatus', '$cancellationReason', '$currentTime')") 
         or die(mysqli_error($connObj));

        return $dbQuery;
    }

    /**
     * CRUD Operation on table ordered_item_list
     */
    public function insertOrderItems($connObj, $orderId,
         $itemID, $itemPrice, $itemQuantity){
            $dbQuery = mysqli_query($connObj, "INSERT INTO ordered_item_list
             (order_id, item_id, item_price, item_quantity)
             VALUES ('$orderId','$itemID','$itemPrice','$itemQuantity')")
             or die(mysqli_error($connObj));

            return $dbQuery;
    }

    /** 
     * JOIN QUERY OPERATIONS
     */

    public function getCustomerPendingOrders($objConn, $regMobileNo){
        $status = "pending";
        $query = "SELECT *, GROUP_CONCAT(OIL.item_id) as item_ids, 
        GROUP_CONCAT(OIL.item_quantity) as item_quantitys, 
        GROUP_CONCAT(OIL.item_price) as item_prices, 
        GROUP_CONCAT(GVFD.item_name_english) as item_name_englishs, 
        GROUP_CONCAT(GVFD.item_name_hindi) as item_name_hindis, 
        GROUP_CONCAT(GVFD.item_name_marathi) as item_name_marathis, 
        GROUP_CONCAT(GVFD.item_sub_category) as item_sub_categorys, 
        GROUP_CONCAT(GVFD.item_base_quantity) as item_base_quantitys,
        GROUP_CONCAT(GVFD.item_image_url_domain) as item_image_url_domains,
        GROUP_CONCAT(GVFD.item_image_url) as item_image_urls
        FROM order_details as OD 
        join ordered_item_list as OIL on OD.order_id = OIL.order_id 
        join generic_vege_fruit_details as GVFD on OIL.item_id= GVFD.item_id 
        where OD.reg_mob_no = '".$regMobileNo."' 
        AND OD.order_status = '".$status."' 
        GROUP BY OD.order_id
        ORDER BY OD.order_placed_at DESC";
        
        $joinQuery = mysqli_query($objConn, $query) 
        or die (mysqli_error($objConn));

        return $joinQuery;
    }

    public function getCustomerAcceptedOrderDetails($objConn, $regMobileNo,
     $offSet, $offSetLimit){
        require_once './CUSTOMER_CONSTANTS.php';
        $status  = STR_ACCEPTED;
        $status1 = STR_OUT_FOR_DELIVERY;
        $query = "SELECT *, GROUP_CONCAT(OIL.item_id) as item_ids, 
        GROUP_CONCAT(OIL.item_quantity) as item_quantitys, 
        GROUP_CONCAT(OIL.item_price) as item_prices, 
        GROUP_CONCAT(GVFD.item_name_english) as item_name_englishs, 
        GROUP_CONCAT(GVFD.item_name_hindi) as item_name_hindis, 
        GROUP_CONCAT(GVFD.item_name_marathi) as item_name_marathis, 
        GROUP_CONCAT(GVFD.item_sub_category) as item_sub_categorys, 
        GROUP_CONCAT(GVFD.item_base_quantity) as item_base_quantitys,
        GROUP_CONCAT(GVFD.item_image_url_domain) as item_image_url_domains,
        GROUP_CONCAT(GVFD.item_image_url) as item_image_urls
        FROM order_details as OD 
        join ordered_item_list as OIL on OD.order_id = OIL.order_id 
        join generic_vege_fruit_details as GVFD on OIL.item_id= GVFD.item_id 
        where OD.reg_mob_no = '".$regMobileNo."' 
        AND OD.order_status IN ('$status', '$status1') 
        GROUP BY OD.order_id
        ORDER BY OD.order_placed_at DESC limit ".$offSet.",".$offSetLimit." ";
        
        $joinQuery = mysqli_query($objConn, $query) 
        or die (mysqli_error($objConn));

        return $joinQuery;
    }

    public function getCustomerCompletedOrdersDetails($objConn, $regMobileNo,
     $offSet, $offSetLimit){
        require_once './CUSTOMER_CONSTANTS.php';
        $status  = STR_COMPLETED;
        $status1 = STR_REJECTED;
        $status2 = STR_CANCELLED;
        $query = "SELECT *, GROUP_CONCAT(OIL.item_id) as item_ids, 
        GROUP_CONCAT(OIL.item_quantity) as item_quantitys, 
        GROUP_CONCAT(OIL.item_price) as item_prices, 
        GROUP_CONCAT(GVFD.item_name_english) as item_name_englishs, 
        GROUP_CONCAT(GVFD.item_name_hindi) as item_name_hindis, 
        GROUP_CONCAT(GVFD.item_name_marathi) as item_name_marathis, 
        GROUP_CONCAT(GVFD.item_sub_category) as item_sub_categorys, 
        GROUP_CONCAT(GVFD.item_base_quantity) as item_base_quantitys,
        GROUP_CONCAT(GVFD.item_image_url_domain) as item_image_url_domains,
        GROUP_CONCAT(GVFD.item_image_url) as item_image_urls
        FROM order_details as OD 
        join ordered_item_list as OIL on OD.order_id = OIL.order_id 
        join generic_vege_fruit_details as GVFD on OIL.item_id= GVFD.item_id 
        where OD.reg_mob_no = '".$regMobileNo."'
        AND OD.order_status IN ('$status', '$status1', '$status2') 
        GROUP BY OD.order_id
        ORDER BY OD.order_placed_at DESC limit ".$offSet.",".$offSetLimit." ";
        
        $joinQuery = mysqli_query($objConn, $query) 
        or die (mysqli_error($objConn));

        return $joinQuery;
    }

}

// $dbQuery = mysqli_query() or die(mysqli_error($connObj));
?>