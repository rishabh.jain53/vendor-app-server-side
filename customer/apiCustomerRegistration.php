<?php
$jsonObj           = file_get_contents('php://input');
$requestParams     = json_decode($jsonObj);
$paramMobileNo     = $requestParams->reg_mobile_no;
$paramFCMToken     = $requestParams->fcm_token;
$paramCustomerName = $requestParams->customer_name;
$objResponse       = array();
if ($paramMobileNo == null || strlen($paramMobileNo) != 10 || $paramCustomerName == null) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 2 required";
} else {
    require_once './utils.php';
    $currentTime = utilsGetDateAndTimeInIST();

    require_once './customer_db_functions.php';
    $custDbObj = new customer_db_functions();
    $connObj   = $custDbObj->getConnectionObj();

    $isNoAlreadyThere = $custDbObj->isMobileNoAlreadyRegistered($connObj, $paramMobileNo);
    if ($isNoAlreadyThere /*&& $paramFCMToken != null*/) {
        $updateToken = $custDbObj->updateCustomerFcmTokenAndCustomerName($connObj, $paramMobileNo,
            $paramFCMToken, $paramCustomerName, $currentTime);
        if ($updateToken) {
            $objResponse["status"]    = "success";
            $objResponse["operation"] = "update";
            
            $getCustomerDetails = $custDbObj->getCustomerDetails($connObj, $paramMobileNo);
            if(mysqli_num_rows($getCustomerDetails) > 0) {
                $row                        = mysqli_fetch_assoc($getCustomerDetails);
                $details["reg_mobile_no"]   = $row["reg_mobile_no"];
                $details["alt_mobile_no"]   = $row["alt_mobile_no"];
                $details["cust_name"]       = $row["cust_name"];
                $details["created_on"]      = $row["created_on"];
                $details["last_updated_on"] = $row["last_updated_on"];
                $details["address"]         = $row["address"];
                $details["locality"]        = $row["locality"];
                $details["city"]            = $row["city"];
                $details["state"]           = $row["state"];
                $details["country"]         = $row["country"];
                $details["pincode"]         = $row["pincode"];
                $details["fav_vendors"]     = $row["fav_vendors"];

                $objResponse["existing_details"]    = $details;
            }
        } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "103";
            $objResponse["error_msg"]  = "Unable to update";
        }
    } else {
        $insertDetails = $custDbObj->insertRegisterCustomer($connObj, $paramMobileNo,
            $paramFCMToken, $paramCustomerName, $currentTime);
        if ($insertDetails) {
            $objResponse["status"]    = "success";
            $objResponse["operation"] = "insert";
        } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "101";
            $objResponse["error_msg"]  = "Unable to insert";
        }
    }
}

echo json_encode($objResponse);
