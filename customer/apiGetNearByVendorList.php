<?php
$jsonObj = file_get_contents("php://input");
$requestParams = json_decode($jsonObj);
$paramMobileNo = $requestParams->reg_mobile_no;
$paramCustLat = $requestParams->lat;
$paramCustLong = $requestParams->long;

$objResponse["vendor_list"] = array();
$objVendor = array();

if($paramMobileNo == null || $paramCustLat==null || $paramCustLong==null) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 3 required";
} else {
    require_once './customer_db_functions.php';
    $custDbObj = new customer_db_functions();
    $objConn = $custDbObj->getConnectionObj();

    $isMobileNoPresent = $custDbObj->isMobileNoAlreadyRegistered($objConn, $paramMobileNo);
    if($isMobileNoPresent){
        $getList = $custDbObj->getNearByVendorList($objConn, 
         $paramCustLat, $paramCustLong);
        if(mysqli_num_rows($getList)>0){
            
            while($row = mysqli_fetch_assoc($getList)){
                $objVendor["reg_mobile_no"]                = $row["reg_mobile_no"];
                $objVendor["alt_mobile_no"]                = $row["alt_mobile_no"];
                $objVendor["vendor_name"]                  = ucfirst($row["vendor_name"]);
                $objVendor["nick_name"]                    = ucfirst($row["nick_name"]);
                $objVendor["address"]                      = $row["address"];
                $objVendor["location_lat"]                 = $row["location_lat"];
                $objVendor["location_long"]                = $row["location_long"];
                $objVendor["vendor_code"]                  = $row["vendor_code"];
                $objVendor["is_providing_home_delivery"]   = $row["is_providing_home_delivery"];
                $objVendor["vendor_rating"]                = $row["vendor_rating"];
                $objVendor["min_waiting_time_in_minutes"]  = $row["min_waiting_time_in_minutes"];
                $objVendor["vendor_category"]              = $row["vendor_category"];
                $objVendor["vendor_image_url"]             = $row["vendor_image_url_domain"] . $row["vendor_image_url"];
                $objVendor["min_order_amount"]             = $row["min_order_amount"];
                $objVendor["delivery_charges"]             = $row["delivery_charges"];
                $objVendor["locality"]                     = $row["locality"];
                $objVendor["city"]                         = $row["city"];
                $objVendor["state"]                        = $row["state"];
                $objVendor["country"]                      = $row["country"];
                $objVendor["pincode"]                      = $row["pincode"];
                $objVendor["time_slots"]                   = $row["time_slots"];
                $objVendor["distance"]                     = $row["distance"];
                $objVendor["is_current_time_in_time_slot"] = isVendorAvailableCurrently($row["time_slots"]);

                array_push($objResponse["vendor_list"], $objVendor);
            }
            $objResponse["status"]    = "success";
        } else {
            $objResponse["status"]      = "success";
            $objResponse["vendor_list"] = "";
            $objResponse["message"] = "No Vendors Nearby";
        }

    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "702";
        $objResponse["error_msg"]  = "Mobile number not registered";
    }
}

echo json_encode($objResponse);



function isVendorAvailableCurrently($arrOfJsonObj) {
    $decodeJsonArr = json_decode($arrOfJsonObj);
    if($decodeJsonArr !=null || count($decodeJsonArr) > 0) {
        require_once './utils.php';
        $currentISTTime = utilsGetTimeInIST();
        foreach($decodeJsonArr as $timeSlot) {
            $startTime = $timeSlot->start_time;
            $endTime = $timeSlot->end_time;
            if($startTime <= $currentISTTime && $currentISTTime <= $endTime){
                return true;
            }
        }
    } else {
        //is available
        return true;
    }
    return false;
}
?>