<?php
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramCustMobileNo = $requestParams->reg_mobile_no;

$objResponse = array();
if($paramCustMobileNo == null) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 1 required";
} else {
    require_once './utils.php';
    require_once './customer_db_functions.php';
    $custDbObj = new customer_db_functions();
    $objConn = $custDbObj->getConnectionObj();

    $isMobileNoPresent = $custDbObj->isMobileNoAlreadyRegistered($objConn, $paramCustMobileNo);
    if($isMobileNoPresent){
        $getPendingOrders = $custDbObj->getCustomerPendingOrders($objConn, $paramCustMobileNo);

        $objResponse["status"] = "success";

        if (mysqli_num_rows($getPendingOrders) > 0) {
            $formattedJson = utilsGetOrdersInJSONFormatNew($getPendingOrders);
            $objResponse["count"] = count($formattedJson);
            $objResponse["orders"] = $formattedJson;
        } else {
            $objResponse["count"] = 0;
            $objResponse["orders"] = "No orders found";
        }
    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "702";
        $objResponse["error_msg"]  = "Mobile number not registered";
    }

}

echo json_encode($objResponse);




?>