<?php
/* {
 "reg_mobile_no": "9769369231",
 "vendor_code": "RI-17944",
 "vendor_category": "grocery",
 "has_opt_for_home_delivery": "false",
 "order_total_amount": "301.0",
 "vendor_mob_no": "9819509645",
 "vendor_name": "rishabh",
 "min_waiting_time_in_minutes": "30",
 "delivery_charges": "0.00",
 "customer_name": "Divyesh",
 "latitude": "72.1224",
 "longitude": "138.2112",
 "address": "address",
 "locality": "locality",
 "city": "Rajkot",
 "state": "Gujarat",
 "country": "India",
 "pincode": "363650",
 "order_list": [
   {
     "item_id": "1",
     "item_price": "12.00",
     "item_quantity": "0"
   },
   {
     "item_id": "4",
     "item_price": "40.00",
     "item_quantity": "7"
   },
   {
     "item_id": "5",
     "item_price": "15.00",
     "item_quantity": "0"
   },
   {
     "item_id": "6",
     "item_price": "21.00",
     "item_quantity": "1"
   }
 ]
}*/
//19 keys in request body
$json                       = file_get_contents("php://input");
$requestParams              = json_decode($json);
$paramMobileNo              = $requestParams->reg_mobile_no;
$paramVendorCode            = $requestParams->vendor_code;
$paramVendorCategory        = $requestParams->vendor_category;
$paramArrOrderList          = $requestParams->order_list;
$paramHasOptForHomeDelivery = $requestParams->has_opt_for_home_delivery;
$paramOrderTotalAmount      = $requestParams->order_total_amount;

$paramCustomerName    = $requestParams->customer_name;
$paramDeliveryCharges = $requestParams->delivery_charges;
$paramMinWaitingTime  = $requestParams->min_waiting_time_in_minutes;
$paramVendorName      = $requestParams->vendor_name;
$paramVendorMobNumber = $requestParams->vendor_mob_no;

$objResponse = array(); 

if ($paramMobileNo == null || strlen($paramMobileNo) != 10 
 || $paramVendorCode == null || $paramVendorCategory == null 
 || $paramArrOrderList == null || $paramHasOptForHomeDelivery == null 
 || $paramOrderTotalAmount == null || $paramCustomerName == null
 || $paramMinWaitingTime == null || $paramVendorName == null
 || $paramVendorMobNumber == null ) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 10 required";
} else {

    $paramHasOptForHomeDelivery = filter_var($paramHasOptForHomeDelivery, FILTER_VALIDATE_BOOLEAN);
    require_once './utils.php';
    require_once './CUSTOMER_CONSTANTS.php';
    require_once './customer_db_functions.php';
    $custDbObj = new customer_db_functions();
    $connObj   = $custDbObj->getConnectionObj();

    $isCustomerBlocked = $custDbObj->isMobileNoBlocked($connObj, $paramMobileNo);
    if(!$isCustomerBlocked){
        $isVendorCodeValid = $custDbObj->isVendorCodePresent($connObj, $paramVendorCode);

        if($isVendorCodeValid){
            $isVendorCurrentlyTakingOrder = $custDbObj->isVendorCurrentlyTakingOrders($connObj,
             $paramVendorCode);

             if($isVendorCurrentlyTakingOrder){

                if($paramHasOptForHomeDelivery){
                    //Home Delivery order

                   $isVendorProvidingHomeDelivery = $custDbObj->isVendorProvidingHomeDelivery($connObj,
                    $paramVendorCode);

                    if ($isVendorProvidingHomeDelivery) {
                        $paramCurrLat  = $requestParams->latitude;
                        $paramCurrLong = $requestParams->longitude;
                        $paramAddress  = $requestParams->address;
                        $paramLocality = $requestParams->locality;
                        $paramCity     = $requestParams->city;
                        $paramState    = $requestParams->state;
                        $paramCountry  = $requestParams->country;
                        $paramPinCode  = $requestParams->pincode;
                        if ($paramCurrLat == null || $paramCurrLong == null
                        || $paramAddress == null || $paramPinCode == null) {
                            $objResponse["status"]     = "error";
                            $objResponse["error_code"] = "710";
                            $objResponse["error_msg"]  = "Dear Customer, please provide the delivery address.";
                        } else {
                        
                            $placeOrder = $custDbObj->insertPlaceOrder($connObj,
                              $paramMobileNo, $paramVendorCode, $paramVendorCategory,
                              $paramHasOptForHomeDelivery, $paramOrderTotalAmount, 
                              $paramCustomerName, $paramMinWaitingTime, 
                              $paramVendorName, $paramVendorMobNumber,
                              $paramCurrLat, $paramCurrLong, $paramAddress,
                              $paramLocality, $paramCity, $paramState, $paramCountry, $paramPinCode,
                              $paramDeliveryCharges);

                            if ($placeOrder) {
                                $orderId = $custDbObj->getLastPlacedOrderID($connObj, $paramMobileNo,
                                 $paramVendorCode, $paramVendorCategory);
                                if ($orderId) {
                                    
                                    $insertOrderDetailsInOrderStatusDetails = 
                                     $custDbObj->insertOrderNewStatusInOrderStatusDetails($connObj, $orderId, 
                                     $paramMobileNo, $paramVendorCode, $paramVendorCategory,
                                     $paramHasOptForHomeDelivery, $paramOrderTotalAmount, 
                                     $paramCustomerName, $paramMinWaitingTime, 
                                     $paramVendorName, $paramVendorMobNumber, STR_PENDING, null,
                                     $paramCurrLat, $paramCurrLong, $paramAddress,
                                     $paramLocality, $paramCity, $paramState, $paramCountry, $paramPinCode,
                                     $paramDeliveryCharges);

                                    $insertItems = fnInsertOrderItems($connObj, $custDbObj,
                                    $orderId, $paramArrOrderList);
                                    if ($insertItems) {
                                        $objResponse["status"]   = "success";
                                        $objResponse["order_id"] = $orderId;
                                    } else {
                                        $objResponse["status"]     = "error";
                                        $objResponse["error_code"] = "101";
                                        $objResponse["error_msg"]  = "unable to insert";
                                    }

                                    $fcmToken = $custDbObj->getVendorFcmToken($connObj, $paramVendorCode);
                                    if($fcmToken != null) {
                                        utilsSendFCMMessageSingle($fcmToken, 
                                        "Home delivery order received", "New order received. Order No. " . $orderId, FIREBASE_KEY_VENDOR, STR_PENDING, STR_PENDING);
                                    }

                                } else {
                                    $objResponse["status"]     = "error";
                                    $objResponse["error_code"] = "2000";
                                    $objResponse["error_msg"]  = "unable to fetch order id";
                                }
                            } else {
                                $objResponse["status"]     = "error";
                                $objResponse["error_code"] = "101";
                                $objResponse["error_msg"]  = "unable to place order";
                            }
                        }
                    } else {
                        $objResponse["status"]     = "error";
                        $objResponse["error_code"] = "701";
                        $objResponse["error_msg"]  = "Dear Customer, the requested vendor is currently not providing home delivery service.";
                    }

                } else {
                    //pick up order
                    $placeOrder = $custDbObj->insertPlaceOrder($connObj, $paramMobileNo, 
                    $paramVendorCode, $paramVendorCategory, $paramHasOptForHomeDelivery,
                    $paramOrderTotalAmount, $paramCustomerName, $paramMinWaitingTime, 
                    $paramVendorName, $paramVendorMobNumber);

                    if($placeOrder){
                        $orderId = $custDbObj->getLastPlacedOrderID($connObj, $paramMobileNo,
                        $paramVendorCode, $paramVendorCategory);
                        if($orderId) {

                            $insertOrderDetailsInOrderStatusDetails = 
                            $custDbObj->insertOrderNewStatusInOrderStatusDetails($connObj, $orderId, 
                            $paramMobileNo, $paramVendorCode, $paramVendorCategory,
                            $paramHasOptForHomeDelivery, $paramOrderTotalAmount, 
                            $paramCustomerName, $paramMinWaitingTime, 
                            $paramVendorName, $paramVendorMobNumber, STR_PENDING);
                            
                            $insertItems = fnInsertOrderItems($connObj, $custDbObj,
                             $orderId, $paramArrOrderList);
                             if($insertItems) {
                                $objResponse["status"] = "success";
                                $objResponse["order_id"] = $orderId;
                             } else {
                                $objResponse["status"] = "error";
                                $objResponse["error_code"] = "101";   
                                $objResponse["error_msg"] = "unable to insert"; 
                             }

                            $fcmToken = $custDbObj->getVendorFcmToken($connObj, $paramVendorCode);
                            if($fcmToken != null) {
                                utilsSendFCMMessageSingle($fcmToken, 
                                        "Pick up order received.", "New order received. Order No. " . $orderId, FIREBASE_KEY_VENDOR, STR_PENDING, STR_PENDING);
                            }

                        } else {
                            $objResponse["status"] = "error";
                            $objResponse["error_code"] = "2000";   
                            $objResponse["error_msg"] = "unable to fetch order id"; 
                        }
                    } else {
                        $objResponse["status"] = "error";
                        $objResponse["error_code"] = "101";   
                        $objResponse["error_msg"] = "unable to place order";
                    }
                }
             } else {
                $objResponse["status"] = "error";
                $objResponse["error_code"] = "700";   
                $objResponse["error_msg"] = "Dear Customer, the requested vendor is currently not taking any orders. Please check vendor's time slots";
             }
        } else {
            $objResponse["status"] = "error";
            $objResponse["error_code"] = "200";   
            $objResponse["error_msg"] = "invalid vendor code"; 
        }

    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "706";
        $objResponse["error_msg"]  = "Dear Customer, your number is reported by the vendor. Please contact the authorized person.";
    }
}

echo json_encode($objResponse);


function fnInsertOrderItems($connObj, $custClassObj, $orderId, $arrOrderList){
    foreach($arrOrderList as $order){
        //TODO in future enhancement key operation_type will be used to CRUD a order item
        $itemID = $order->item_id;
        $itemPrice = $order->item_price;
        $itemQuantity = $order->item_quantity;
        $insertOrderItem = $custClassObj->insertOrderItems($connObj, $orderId,
         $itemID, $itemPrice, $itemQuantity);
         if(!$insertOrderItem){
            return false;
         }
    }
    return true;
}

?>