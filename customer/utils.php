<?php

function utilsGetDateAndTimeInIST(){
    date_default_timezone_set('Asia/Kolkata');
    $currentTime = time();
    $currentTime = date("Y-m-d H:i:s", $currentTime);
    return $currentTime;
}

function utilsGetTimeInIST() {
    date_default_timezone_set('Asia/Kolkata');
    $currentTime = time();
    return date("H:i", $currentTime);
}

function utilsGetCurrentTime() {
    $currentTime = time();
    $currentTime = date("Y-m-d H:i:s", $currentTime); 
    return $currentTime;
}

function utilsGetOrdersInJSONFormatNew($queryResult){
    $arrOfOrders = array();
    while($row = mysqli_fetch_assoc($queryResult)){
            $orderObj["item_list"] = array();
            $arrOfItemListPerOrder = array();

            $orderObj["order_id"]           = $row["order_id"];
            $orderObj["customer_mobile_no"] = $row["reg_mob_no"];
            $orderObj["vendor_code"] = $row["vendor_code"];
            $hasOptForDelivery           = ($row["has_opt_for_home_delivery"]) ? true : false;
            $orderObj["has_opt_for_home_delivery"] = $hasOptForDelivery;
            if ($hasOptForDelivery) {
                $orderObj["delivery_lat"]  = $row["delivery_lat"];
                $orderObj["delivery_long"] = $row["delivery_long"];
                $orderObj["address"]       = $row["address"];
                $orderObj["locality"]      = $row["locality"];
                $orderObj["city"]          = $row["city"];
                $orderObj["state"]         = $row["state"];
                $orderObj["country"]       = $row["country"];
                $orderObj["pin_code"]      = $row["pin_code"];
            }

            $orderObj["order_status"] = $row["order_status"];
            $orderObj["vendor_category"] = $row["vendor_category"];
            $orderObj["delivery_charges"] = $row["delivery_charges"];
            $orderObj["total_amount"]     = $row["total_amount"];
            $orderObj["customer_cancellation_reason"] = $row["customer_cancellation_reason"];
            $orderObj["vendor_cancellation_reason"] = $row["vendor_cancellation_reason"];
            $orderObj["rating"] = $row["rating"];
            $orderObj["remark"] = $row["remark"];
            $orderObj["order_placed_at"] = $row["order_placed_at"];
            $orderObj["order_last_updated"] = $row["order_last_updated"];
            $orderObj["customer_name"] = $row["customer_name"];
            $orderObj["min_waiting_time_in_minutes"] = $row["min_waiting_time_in_minutes"];
            $orderObj["vendor_name"] = $row["vendor_name"];
            $orderObj["vendor_mobile_no"] = $row["vendor_number"];

            $arrItemIds             = explode(",", $row["item_ids"]);
            $count                  = count($arrItemIds);
            $arrItemQuantitys       = explode(",", $row["item_quantitys"]);
            $arrItemPrices          = explode(",", $row["item_prices"]);
            $arrItemEnglishs        = explode(",", $row["item_name_englishs"]);
            $arrItemHindis          = explode(",", $row["item_name_hindis"]);
            $arrItemMarathis        = explode(",", $row["item_name_marathis"]);
            $arrItemSubCategorys    = explode(",", $row["item_sub_categorys"]);
            $arrItemBaseQuantitys   = explode(",", $row["item_base_quantitys"]);
            $arrItemImageUrlDomains = explode(",", $row["item_image_url_domains"]);
            $arrItemImageUrls       = explode(",", $row["item_image_urls"]);

            for ($i=0; $i<$count; $i++) {
                $objOfEachItem = array();
                $objOfEachItem["item_id"]            = $arrItemIds[$i];
                $objOfEachItem["item_quantity"]      = $arrItemQuantitys[$i];
                $objOfEachItem["item_price"]         = $arrItemPrices[$i];
                $objOfEachItem["item_name_english"]  = $arrItemEnglishs[$i];
                $objOfEachItem["item_name_hindi"]    = $arrItemHindis[$i];
                $objOfEachItem["item_name_marathi"]  = $arrItemMarathis[$i];
                $objOfEachItem["item_sub_category"]  = $arrItemSubCategorys[$i];
                $objOfEachItem["item_base_quantity"] = $arrItemBaseQuantitys[$i];
                $objOfEachItem["item_image_url"]     = $arrItemImageUrlDomains[$i].$arrItemImageUrls[$i];
                array_push($arrOfItemListPerOrder, $objOfEachItem);
            }

        $orderObj["item_list"] = $arrOfItemListPerOrder;
        array_push($arrOfOrders, $orderObj);
                     
        }
        
    return $arrOfOrders;
}

function utilsSendFCMMessageSingle($singleToken, $notiBodyMsg,
 $notiTitle, $serverKey, $newOrderStatus, $oldOrderStatus = null)
 {
  //FCM API end-point
  $url = 'https://fcm.googleapis.com/fcm/send';

  //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
  $server_key = $serverKey;


  $fields = array(
   'to'           => $singleToken,
   'data' => array(
    "body"  => $notiBodyMsg,
    "title" => $notiTitle,
    "new_order_status"  => $newOrderStatus,
    "old_order_status" => $oldOrderStatus
   ),
  );

  //header with content_type api key
  $headers = array
   (
   'Content-Type:application/json',
   'Authorization:key=' . $server_key,
  );

  //CURL request to route notification to FCM connection server (provided by Google)
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
  $result = curl_exec($ch);
  if ($result === false) {
   die('Oops! FCM Send Error: ' . curl_error($ch));
  }
  curl_close($ch);

  return $result;
 }

?>