<?php
//20 keys in request body
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramCustMobileNo = $requestParams->reg_mobile_no;
$paramOrderId = $requestParams->order_id;
$paramOrderCancellationReason = $requestParams->cancellation_reason;

$paramVendorCode = $requestParams->vendor_code;

$paramVendorCategory        = $requestParams->vendor_category;

$paramHasOptForHomeDelivery = $requestParams->has_opt_for_home_delivery;
$paramOrderTotalAmount      = $requestParams->order_total_amount;

$paramCustomerName    = $requestParams->customer_name;
$paramDeliveryCharges = $requestParams->delivery_charges;
$paramMinWaitingTime  = $requestParams->min_waiting_time_in_minutes;
$paramVendorName      = $requestParams->vendor_name;
$paramVendorMobileNo  = $requestParams->vendor_number;

$paramHasOptForHomeDelivery = filter_var($paramHasOptForHomeDelivery , FILTER_VALIDATE_BOOLEAN); 
if($paramHasOptForHomeDelivery) {
    $paramCurrLat  = $requestParams->latitude;
    $paramCurrLong = $requestParams->longitude;
    $paramAddress  = $requestParams->address;
    $paramLocality = $requestParams->locality;
    $paramCity     = $requestParams->city;
    $paramState    = $requestParams->state;
    $paramCountry  = $requestParams->country;
    $paramPinCode  = $requestParams->pincode;
}
$objResponse = array();
if($paramCustMobileNo == null || $paramOrderId == null 
 || $paramOrderCancellationReason == null) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 1 required";
} else {
    require_once './CUSTOMER_CONSTANTS.php';
    require_once './utils.php';
    require_once './customer_db_functions.php';
    $custDbObj = new customer_db_functions();
    $objConn = $custDbObj->getConnectionObj();

    $doesOrderIdAndCustomerNoMatch = $custDbObj->isOrderIdAssociatedWithSameMobileNo($objConn,
     $paramCustMobileNo, $paramOrderId);
    if($doesOrderIdAndCustomerNoMatch){
        $currentTime = utilsGetDateAndTimeInIST();
        $isOrderInPendingState = $custDbObj->isOrderInParticularStatus($objConn,
         $paramOrderId, STR_PENDING);

        if($isOrderInPendingState) {
            $cancelOrder = $custDbObj-> updateOrderStatus($objConn, $paramOrderId,
             STR_CANCELLED, $currentTime, $paramOrderCancellationReason);
            if($cancelOrder) {
                $insertOrderDetailsInOrderStatusDetails = 
                 $custDbObj->insertOrderNewStatusInOrderStatusDetails($objConn, $paramOrderId, 
                    $paramCustMobileNo, $paramVendorCode, $paramVendorCategory,
                    $paramHasOptForHomeDelivery, $paramOrderTotalAmount, 
                    $paramCustomerName, $paramMinWaitingTime, 
                    $paramVendorName, $paramVendorMobileNo, STR_CANCELLED,
                    $paramOrderCancellationReason, $paramCurrLat,
                    $paramCurrLong, $paramAddress,
                    $paramLocality, $paramCity, $paramState,
                    $paramCountry, $paramPinCode,
                    $paramDeliveryCharges);

                    $objResponse["status"]     = "success";

                    $fcmToken = $custDbObj->getVendorFcmToken($objConn, $paramVendorCode);
                    if($fcmToken != null) {
                        utilsSendFCMMessageSingle($fcmToken, "Customer has cancelled the order.", 
                        "Update for Order No. " . $paramOrderId, FIREBASE_KEY_VENDOR, STR_CANCELLED, STR_PENDING);
                    }
            } else {
                $objResponse["status"]     = "error";
                $objResponse["error_code"] = "103";
                $objResponse["error_msg"]  = "Unable to Update the status";
            }
            
        } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "704";
            $objResponse["error_msg"]  = "Dear Customer, you can't cancel the order now because the vendor has already accepted your order and is processing it.";
        }

    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "703";
        $objResponse["error_msg"]  = "This order id doesn't belong to this customer";
    }

}

echo json_encode($objResponse);

?>