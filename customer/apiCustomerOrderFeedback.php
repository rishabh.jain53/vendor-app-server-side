<?php
//6 keys in request body
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramCustMobileNo = $requestParams->reg_mobile_no;
$paramOrderId = $requestParams->order_id;
$paramAverageRating = $requestParams->average_rating;
$paramFeedbackInDetail = $requestParams->feedback_details;
$paramRemarkIfAny = $requestParams->order_remark;
$paramVendorCode = $requestParams->vendor_code;

// $paramVendorCategory        = $requestParams->vendor_category;
// $paramHasOptForHomeDelivery = $requestParams->has_opt_for_home_delivery;
// $paramOrderTotalAmount      = $requestParams->order_total_amount;
// $paramCustomerName    = $requestParams->customer_name;
// $paramDeliveryCharges = $requestParams->delivery_charges;
// $paramMinWaitingTime  = $requestParams->min_waiting_time_in_minutes;
// $paramVendorName      = $requestParams->vendor_name;
// $paramVendorMobileNo  = $requestParams->vendor_number;

// $paramHasOptForHomeDelivery = filter_var($paramHasOptForHomeDelivery , FILTER_VALIDATE_BOOLEAN); 
// if($paramHasOptForHomeDelivery) {
//     $paramCurrLat  = $requestParams->latitude;
//     $paramCurrLong = $requestParams->longitude;
//     $paramAddress  = $requestParams->address;
//     $paramLocality = $requestParams->locality;
//     $paramCity     = $requestParams->city;
//     $paramState    = $requestParams->state;
//     $paramCountry  = $requestParams->country;
//     $paramPinCode  = $requestParams->pincode;
// }
$objResponse = array();
if($paramCustMobileNo == null || $paramOrderId == null 
 || $paramAverageRating == null || $paramFeedbackInDetail == null 
 || $paramAverageRating > 5 || $paramAverageRating < 1) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "4";
    $objResponse["error_msg"]  = "Invalid request parameters 4 required the rating cannot be below 1 or above 5";
} else {
    require_once './CUSTOMER_CONSTANTS.php';
    require_once './utils.php';
    require_once './customer_db_functions.php';
    $custDbObj = new customer_db_functions();
    $objConn = $custDbObj->getConnectionObj();

    $doesOrderIdAndCustomerNoMatch = $custDbObj->isOrderIdAssociatedWithSameMobileNo($objConn,
     $paramCustMobileNo, $paramOrderId);
    if($doesOrderIdAndCustomerNoMatch){

        $currentTime = utilsGetDateAndTimeInIST();
        $isOrderInCompletedState = $custDbObj->isOrderInParticularStatus($objConn,
         $paramOrderId, STR_COMPLETED);

        if(TRUE) {
            $paramAverageRating = floatval($paramAverageRating);

            $setRatingOfOrder = $custDbObj->setOrderFeedbackRating($objConn, $paramOrderId, 
             $paramAverageRating, $paramFeedbackInDetail, $paramRemarkIfAny, $currentTime);
            if($setRatingOfOrder){
                $objResponse["status"]     = "success";
                $getVendorOverAllRating = $custDbObj->getVendorRatings($objConn, $paramVendorCode);
                $getVendorOverAllRating = floatval($getVendorOverAllRating);
                
                if($getVendorOverAllRating == 0 || $getVendorOverAllRating == 0.0) {
                    $newRating = $paramAverageRating;
                } else {
                    $newRating = ($getVendorOverAllRating + $paramAverageRating) / 2;
                }
                
                $updateVendorRating = $custDbObj->updateVendorRating($objConn, 
                 $paramVendorCode, $newRating);
                
            } else {
                $objResponse["status"]     = "error";
                $objResponse["error_code"] = "103";
                $objResponse["error_msg"]  = "Unable to Update the feedback for the order";
            }

        } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "705";
            $objResponse["error_msg"]  = "Unable to rate the order because it is not completed yet";
        }
    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "703";
        $objResponse["error_msg"]  = "This order id doesn't belong to this customer";
    }
}

echo json_encode($objResponse);

?>