<?php
$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramCustMobileNo = $requestParams->reg_mobile_no;
$paramOrderStatus = $requestParams->order_status;
$paramOrderStatus = trim($paramOrderStatus);
$paramPageNo = $requestParams->page_no;
$offSetLimit = 10;
$offset = $paramPageNo * $offSetLimit;

$objResponse = array();
if($paramCustMobileNo == null || $paramOrderStatus == null 
   || $paramPageNo == null) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 3 required";
} else {
    require_once './CUSTOMER_CONSTANTS.php';
    require_once './utils.php';
    require_once './customer_db_functions.php';
    $custDbObj = new customer_db_functions();
    $objConn = $custDbObj->getConnectionObj();

    $isMobileNoPresent = $custDbObj->isMobileNoAlreadyRegistered($objConn, $paramCustMobileNo);
    if($isMobileNoPresent){
        $getOrders;
        switch($paramOrderStatus){
            case STR_ACCEPTED:
                $getOrders = $custDbObj->getCustomerAcceptedOrderDetails($objConn,
                 $paramCustMobileNo, $offset, $offSetLimit);
            break;
            case STR_COMPLETED:
                $getOrders = $custDbObj->getCustomerCompletedOrdersDetails($objConn,
                 $paramCustMobileNo, $offset, $offSetLimit);
            break;
            default:
                $objResponse["status"] = "error";
                $objResponse["error_code"] = "1";
                $objResponse["error_msg"]  = "Invalid request parameter status not found";
                echo json_encode($objResponse);
                exit();
            break;
        }

        $objResponse["status"] = "success";

        if (mysqli_num_rows($getOrders) > 0) {
            $formattedJson = utilsGetOrdersInJSONFormatNew($getOrders);
            $objResponse["count"] = count($formattedJson);
            $objResponse["orders"] = $formattedJson;
        } else {
            $objResponse["count"] = 0;
            $objResponse["orders"] = "No orders found";
        }
    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "702";
        $objResponse["error_msg"]  = "Mobile number not registered";
    }

}

echo json_encode($objResponse);

?>