<?php
$jsonObj       = file_get_contents('php://input');
$requestParams = json_decode($jsonObj);
$paramFCMToken = $requestParams->fcm_token;    
$paramCustMobileNo = $requestParams->reg_mobile_no;

$objResponse = array();

if ($paramCustMobileNo == null || $paramFCMToken == null) {
    //error
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 2 required";
} else {
    require_once './customer_db_functions.php';
    $custDbObj = new customer_db_functions();
    $objConn = $custDbObj->getConnectionObj();

    $isMobileNoPresent = $custDbObj->isMobileNoAlreadyRegistered($objConn, $paramCustMobileNo);
    if($isMobileNoPresent){
        require_once './utils.php';
        $currentTime              = utilsGetDateAndTimeInIST();
        $updateToken = $custDbObj->updateCustomerFcmToken($objConn, $paramCustMobileNo,
            $paramFCMToken, $currentTime);
        if ($updateToken) {
            $objResponse["status"]    = "success";
            $objResponse["operation"] = "update";
        } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "103";
            $objResponse["error_msg"]  = "Unable to update";
        }
    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "702";
        $objResponse["error_msg"]  = "Mobile number not registered";
    }

}

echo json_encode($objResponse);

?>