<?php
$jsonObj           = file_get_contents('php://input');
$requestParams     = json_decode($jsonObj);
$paramMobileNo     = $requestParams->reg_mobile_no;

$objResponse       = array();
if ($paramMobileNo == null || strlen($paramMobileNo) != 10 ) {
    $objResponse["status"]     = "error";
    $objResponse["error_code"] = "1";
    $objResponse["error_msg"]  = "Invalid request parameters 1 required";
} else {
    require_once './utils.php';
    $currentTime = utilsGetDateAndTimeInIST();

    require_once './customer_db_functions.php';
    $custDbObj = new customer_db_functions();
    $connObj   = $custDbObj->getConnectionObj();

    $isNoAlreadyThere = $custDbObj->isMobileNoAlreadyRegistered($connObj, $paramMobileNo);
    if ($isNoAlreadyThere) {
        $paramAltMobileNo = $requestParams->alt_mobile_no;
        $paramCustomerName = $requestParams->customer_name;
        $paramAddress = $requestParams->address;
        $paramLocality = $requestParams->locality;
        $paramCity = $requestParams->city;
        $paramState = $requestParams->state;
        $paramCountry = $requestParams->country;
        $paramPincode = $requestParams->pincode;

        $updateDetails = $custDbObj->updateCustomerDetails($connObj, $paramMobileNo,
         $paramAltMobileNo, $paramCustomerName, $paramAddress, $paramLocality, $paramCity,
         $paramState, $paramCountry, $paramPincode, $currentTime);

        if($updateDetails){
            $objResponse["status"]     = "success";
        } else {
            $objResponse["status"]     = "error";
            $objResponse["error_code"] = "103";
            $objResponse["error_msg"]  = "Unable to update";
        }
    } else {
        $objResponse["status"]     = "error";
        $objResponse["error_code"] = "702";
        $objResponse["error_msg"]  = "Mobile number not registered";
    }
}

echo json_encode($objResponse);