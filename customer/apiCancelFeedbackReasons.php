<?php

// {
//     "cancel": [
//       "cancel Reason 1",
//       "cancel Reason 2",
//       "cancel Reason 3",
//       "cancel Reason 4"
//     ],
//     "feedback": [
//       "feedback option 1",
//       "feedback option 2",
//       "feedback option 3",
//       "feedback option 4"
//     ]
// }

$jsonObj         = file_get_contents("php://input");
$requestParams   = json_decode($jsonObj);
$paramClientLastSavedTime = $requestParams->client_saved_time;

$objResponse = array();

$serverLastSavedTime = "2020-07-01 00:00:00";
if($paramClientLastSavedTime == $serverLastSavedTime) {
       $objResponse["status"] = "noupdate";
} else {
       //Cancel English
       $arrCancel = array("Order placed by mistake",
              "Currently order items are not required",
              "Currently not available to receive or pick up order",
              "Order preparation time is too much");
       $objResponse["cancel"] = array();
       foreach ($arrCancel as $cancel) {
              array_push($objResponse["cancel"], $cancel);
       }

       // //Cancel Hindi
       // $arrCancelHindi = array("Order placed by mistake",
       //        "Currently order items are not required",
       //        "Currently not available to receive or pick up order",
       //        "Order preparation time is too much");
       // $objResponse["cancel_hindi"] = array();
       // foreach ($arrCancelHindi as $cancel) {
       //        array_push($objResponse["cancel_hindi"], $cancel);
       // }

       // //Cancel Marathi
       // $arrCancelMarathi = array("Order placed by mistake",
       //        "Currently order items are not required",
       //        "Currently not available to receive or pick up order",
       //        "Order preparation time is too much");
       // $objResponse["cancel_marathi"] = array();
       // foreach ($arrCancelMarathi as $cancel) {
       //        array_push($objResponse["cancel_marathi"], $cancel);
       // }

       //feedback English
       $arrFeedback = array("Ordered Item Quality",
              "Vendor Service",
              "Ordered Item Price",
              "Overall Experience");
       $objResponse["feedback"] = array();
       foreach ($arrFeedback as $feedback) {
              array_push($objResponse["feedback"], $feedback);
       }

       // //feedback hindi
       // $arrFeedbackHindi = array("feedback option 1 hindi",
       //        "feedback option 2",
       //        "feedback option 3",
       //        "feedback option 4");
       // $objResponse["feedback_hindi"] = array();
       // foreach ($arrFeedbackHindi as $feedback) {
       //        array_push($objResponse["feedback_hindi"], $feedback);
       // }

       // //feedback Marathi
       // $arrFeedbackMarathi = array("feedback option 1 Marathi",
       //        "feedback option 2",
       //        "feedback option 3",
       //        "feedback option 4");
       // $objResponse["feedback_marathi"] = array();
       // foreach ($arrFeedbackMarathi as $feedback) {
       //        array_push($objResponse["feedback_marathi"], $feedback);
       // }
       $objResponse["status"] = "success";
       $objResponse["server_time"] = $serverLastSavedTime;
}

echo json_encode($objResponse);
