<?php

class db_connect
{

 public function connect()
 {

  // import database connection variables
  require_once 'db_config.php';
  // Connecting to mysql database
  $connObj = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE, DB_PORT)
  or die(mysqli_connect_error($connObj));
  return $connObj;

 }

 public function close()
 {
  mysqli_close();
 }

}
