<?php
//Status key word codes - NC: No change C: Changed

$json                      = file_get_contents('php://input');
$requestParams             = json_decode($json);
$paramClientSideSavedDate = $requestParams->client_date;
$paramClientSideSavedTime = $requestParams->client_time;

$responseArray = array();

//$serverLastUpdatedDate = "09/01/2019";  last updated on
$serverLastUpdatedDate = "2020-05-05"; // EDITABLE //YYYY-MM-DD

//$serverLastUpdatedTime = "01:52:00"; // EDITABLE //HH:MM:SS
$serverLastUpdatedTime = "01:52:00"; // EDITABLE //HH:MM:SS

$serverApiVersion      = "1.0.1"; //update everytime you change something
if ($paramClientSideSavedDate == null
    && $paramClientSideSavedTime == null) {
    $responseArray["error"] = "Invalid date or time";
    echo json_encode($responseArray);
} else {
    if ($paramClientSideSavedDate == $serverLastUpdatedDate
        && $paramClientSideSavedTime == $serverLastUpdatedTime) {
        $responseArray["is_changed"] = false;
        echo json_encode($responseArray);
    } else {
        setList($serverLastUpdatedDate, $serverLastUpdatedTime);
    }
}

function setList($serverLastUpdatedDate, $serverLastUpdatedTime) {
    $responseArray["is_changed"]                   = true;
    $responseArray["server_last_updated_date"]     = $serverLastUpdatedDate;
    $responseArray["server_last_updated_time"]     = $serverLastUpdatedTime;
    $responseArray["update_insert_vendor_details"] = "http://maximatech.in/vendormanagement/vendor/apiUpdateVendorDetails.php";

    echo json_encode($responseArray);
}
